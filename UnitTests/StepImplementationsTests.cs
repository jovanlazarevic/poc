using MCC.SeleniumFlowFactory;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class StepImplementationsTests
    {
        [Fact]
        public void GetByXPath_1()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "AboutLink"));

            // Step needed for passing SyntaxChecker
            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            var defName = GetCurrentMethod();
            var defUrl = "https://www.google.com/";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "https://www.google.com/", 3, true);
            ExecuteAlgorithm(flowState);

            Assert.False(flowState.Exceptions.Count > 0);

            var result = flowState.Results.FirstOrDefault(x => x.Key == "AboutLink");
            Assert.True(result.Value != null);
            Assert.True(result.Value is IWebElement);
        }

        #region ForEach
        [Fact]
        public void ForEach_GetElementText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            var defName = GetCurrentMethod();
            var defUrl = "https://www.google.com/";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "https://www.google.com/", 3, true);
            ExecuteAlgorithm(flowState);

            Assert.False(flowState.Exceptions.Count > 0);

            var result = flowState.Results.Where(x => x.Value is List<KeyValuePair<string, object>>).ToList();
            Assert.True(result.Count > 1);
        }

        [Fact]
        public void ForEach_GetElementText_GetElementAttribute()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            var defName = GetCurrentMethod();
            var defUrl = "https://www.google.com/";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "https://www.google.com/", 3, true);
            ExecuteAlgorithm(flowState);

            Assert.False(flowState.Exceptions.Count > 0);

            var result = flowState.Results.Where(x => x.Value is List<KeyValuePair<string, object>>).ToList();
            Assert.True(result.Count > 1);
        }

        [Fact]
        public void ForEach_GetElementText_StringReplace()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//button[@id = 'button1' and @type = 'submit' and (text() = 'Click Me!' or . = 'Click Me!')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));


            defs.Add(new StepDefinition(Constants.StepType.StringReplace));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.FindString, "Click Me!"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ReplaceString, "Google"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            var defName = GetCurrentMethod();
            var defUrl = "https://www.ultimateqa.com/simple-html-elements-for-automation/";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "https://www.ultimateqa.com/simple-html-elements-for-automation/", 3, true);
            ExecuteAlgorithm(flowState);

            Assert.False(flowState.Exceptions.Count > 0);

            var result = flowState.Results.Where(x => x.Value is List<KeyValuePair<string, object>>).ToList();
            Assert.True(result.Count > 0);
            var result2 = flowState.Results[1].ToString();
            Assert.Contains("Google", result2);
        }

        [Fact]
        public void Click_Wait_If()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//button[@id = 'button1' and @type = 'submit' and (text() = 'Click Me!' or . = 'Click Me!')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));

            {
                defs.Add(new StepDefinition(Constants.StepType.Click));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));

                defs.Add(new StepDefinition(Constants.StepType.Wait));               
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "5"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a[@href = 'https://courses.ultimateqa.com' and (text() = 'Start learning now' or . = 'Start learning now')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NewPageButton"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NewPageButton"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));
            }

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            var defName = GetCurrentMethod();
            var defUrl = "https://www.ultimateqa.com/simple-html-elements-for-automation/";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "https://www.ultimateqa.com/simple-html-elements-for-automation/", 3, true);
            ExecuteAlgorithm(flowState);

            Assert.False(flowState.Exceptions.Count > 0);

            var result = flowState.Results.Where(x => x.Value is List<KeyValuePair<string, object>>).ToList();
            Assert.True(result.Count > 0);
            var result2 = flowState.Results[2].ToString();
            Assert.Contains("Start learning now", result2);
        }
        #endregion

        #region Helper Methods

        private void ExecuteAlgorithm(FlowStateSelenium flowState)
        {
            Console.WriteLine($"{DateTime.UtcNow}");
            try
            {
                // Process Work
                FlowParentSelenium fp = new FlowParentSelenium();
                if (fp.PrepareAndCheck(flowState))
                {
                    fp.ProcessWork();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        #endregion
    }
}
