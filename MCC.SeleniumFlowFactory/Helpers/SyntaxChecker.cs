﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MCC.SeleniumFlowFactory.Helpers
{
    public class SyntaxChecker
    {
        public static bool ValidateSyntax(FlowStateSelenium flowState)
        {
            var result = true;

            try
            {
                if (flowState.StepDefinitions.OrderBy(x => x.StepNumber).First().TypeName != Constants.StepType.GoToUrl)
                {
                    flowState.AddException(new Exception("First algorithm step has to be 'GoToUrl'"));
                    result = false;
                }

                result &= ValidateStepParams(flowState);
                result &= CheckInputParams(flowState);
                result &= CheckOptionalParams(flowState);
                result &= CheckBeginEndPairs(flowState);
                result &= CheckResultNames(flowState);
            }
            catch (Exception ex)
            {
                flowState.AddException(new Exception($"Exception during syntax check {Environment.NewLine}: {ex.ToString()}"));
            }

            return result;
        }

        private static bool ValidateStepParams(FlowStateSelenium flowState)
        {
            var result = true;
            if (flowState.StepDefinitions.Count > 0)
            {
                foreach (var def in flowState.StepDefinitions)
                {
                    var assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    var instance = Assembly.GetExecutingAssembly().CreateInstance($"{assemblyName}.{def.TypeName}");
                    ((Step)instance).StepDefinition = def;
                    var errors = ((Step)instance).CheckParams();
                    foreach (var e in errors)
                    {
                        flowState.AddException(new Exception(e));
                    }
                }
                if (flowState.Exceptions.Any())
                {
                    result = false;
                }
            }
            return result;
        }

        private static bool CheckInputParams(FlowStateSelenium flowState)
        {
            var result = true;

            foreach (var def in flowState.StepDefinitions)
            {
                var inputParam = def.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputObject);
                if (inputParam.Key != null)
                {
                    var stepWithResult = flowState.StepDefinitions.FirstOrDefault(x => x.StepNumber < def.StepNumber &&
                                                                                    x.Params.Any(y => y.Key == Constants.ParamType.AddOrUpdateResult && y.Value == inputParam.Value));
                    if (stepWithResult == null)
                    {
                        flowState.AddException(new Exception($"Step {def.StepNumber} {def.TypeName} expect input object {inputParam.Value} which was not specified before."));
                        result = false;
                    }
                }
            }

            return result;
        }

        private static bool CheckOptionalParams(FlowStateSelenium flowState)
        {
            var result = true;

            foreach (var def in flowState.StepDefinitions)
            {
                var count = 1;
                switch (def.TypeName)
                {
                    case Constants.StepType.If:
                        count = def.Params.Count(x => x.Key == Constants.ParamType.EqualsTo ||
                                                                         x.Key == Constants.ParamType.NotEqualsTo ||
                                                                         x.Key == Constants.ParamType.ContainsIn ||
                                                                         x.Key == Constants.ParamType.EndsWith ||
                                                                         x.Key == Constants.ParamType.NotEndsWith ||
                                                                         x.Key == Constants.ParamType.EqualsToObject ||
                                                                         x.Key == Constants.ParamType.NotEqualsToObject ||
                                                                         x.Key == Constants.ParamType.NotContainsInResultsKeys ||
                                                                         x.Key == Constants.ParamType.IsVisible ||
                                                                         x.Key == Constants.ParamType.IsNotVisible);
                        result &= CheckOptionalParamsCount(flowState, def, count);
                        break;
                    case Constants.StepType.While:
                        count = def.Params.Count(x => x.Key == Constants.ParamType.EqualsTo ||
                                                           x.Key == Constants.ParamType.ContainsIn ||
                                                           x.Key == Constants.ParamType.NotEqualsTo ||
                                                           x.Key == Constants.ParamType.EqualsToObject ||
                                                           x.Key == Constants.ParamType.NotEqualsToObject);
                        result &= CheckOptionalParamsCount(flowState, def, count);
                        break;
                }

            }

            return result;
        }

        private static bool CheckOptionalParamsCount(FlowStateSelenium flowState, StepDefinition def, int count)
        {
            var result = true;
            if (count == 0)
            {
                flowState.AddException(new Exception($"Step {def.StepNumber} ({def.TypeName}) expects one \"compare\" parameter."));
                result = false;
            }
            else if (count > 1)
            {
                flowState.AddException(new Exception($"Step {def.StepNumber} ({def.TypeName}) expects only one \"compare\" parameter."));
                result = false;
            }
            return result;
        }

        private static bool CheckBeginEndPairs(FlowStateSelenium flowState)
        {
            var result = true;

            if (flowState.StepDefinitions.Count > 0)
            {
                if (flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.ForEach) !=
                    flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.ForEachEnd))
                {
                    flowState.AddException(new Exception("Not matched 'ForEach' and 'ForEachEnd' steps"));
                    result = false;
                }
                if (flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.While) !=
                    flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.WhileEnd))
                {
                    flowState.AddException(new Exception("Not matched 'While' and 'WhileEnd' steps"));
                    result = false;
                }
                if (flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.If) !=
                    flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.EndIf))
                {
                    flowState.AddException(new Exception("Not matched 'If' and 'EndIf' steps"));
                    result = false;
                }
            }

            return result;
        }

        private static bool CheckResultNames(FlowStateSelenium flowState)
        {
            var result = true;

            if (flowState.StepDefinitions.Count(x => x.TypeName == Constants.StepType.MakeResultObject) <= 0)
            {
                flowState.AddException(new Exception("At least one step in algorithm has to be MakeResultObject"));
                result = false;
            }

            foreach (var r in flowState.StepDefinitions.Where(x => x.TypeName == Constants.StepType.MakeResultObject))
            {
                var inputResultsParam = r.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputResults);
                var addToResultsReplaceKeysParam = r.Params.FirstOrDefault(x => x.Key == Constants.ParamType.AddToResultsReplaceKeys);
                var addToResultsParam = r.Params.FirstOrDefault(x => x.Key == Constants.ParamType.AddToResults);
                switch (flowState.DefinitionType)
                {
                    case "Locations":
                        if ((!ParamEqualsTo(inputResultsParam, "LocationName,LocationHref,LocationState,LocationCounty") && !ParamEqualsTo(addToResultsReplaceKeysParam, "LocationName,LocationHref,LocationState,LocationCounty")) ||
                            !ParamEqualsTo(addToResultsParam, "Location"))
                        {
                            flowState.AddException(new Exception("MakeResultObject step has to have param InputResults (or AddToResultsReplaceKeys): 'LocationName,LocationHref,LocationState,LocationCounty'   and   param AddToResults: 'Location' "));
                            result = false;
                        }
                        break;
                    case "SubUrls":
                        if ((!ParamEqualsTo(inputResultsParam, "Name,Href,HtmlHash") && !ParamEqualsTo(addToResultsReplaceKeysParam, "Name,Href,HtmlHash")) ||
                            !ParamEqualsTo(addToResultsParam, "SubUrl"))
                        {
                            flowState.AddException(new Exception("MakeResultObject step has to have param param InputResults  (or AddToResultsReplaceKeys): 'Name,Href,HtmlHash'  and  param AddToResults : 'SubUrl'"));
                            result = false;
                        }
                        break;
                    case "ScrapingText":
                        var inputResultsParamValue = inputResultsParam.Value?.Replace(" ", "");
                        if ((!ParamEqualsTo(inputResultsParam, "RootUrl,ContentText,ContentHtml") && !ParamEqualsTo(addToResultsReplaceKeysParam, "RootUrl,ContentText,ContentHtml") &&
                            (!ParamEqualsTo(inputResultsParam, "RootUrl,ContentText,ContentStream") && !ParamEqualsTo(addToResultsReplaceKeysParam, "RootUrl,ContentText,ContentStream"))) ||
                            !ParamEqualsTo(addToResultsParam, "ScrapedContent"))
                        {
                            flowState.AddException(new Exception("MakeResultObject step has to have param  param InputResults (or AddToResultsReplaceKeys): 'RootUrl,ContentText,ContentHtml' or 'RootUrl,ContentText,ContentStream'   and   param AddToResults: 'ScrapedContent'"));
                            result = false;
                        }
                        break;
                    default:
                        flowState.AddException(new Exception("Algorithm definition type not in expected: 'Locations', 'SubUrls', 'ScrapingText'"));
                        result = false;
                        break;
                }
            }

            return result;
        }

        private static bool ParamEqualsTo(KeyValuePair<string, string> param, string compareTo)
        {
            var result = false;
            var paramValue = !string.IsNullOrEmpty(param.Value) ? param.Value?.Replace(" ", "") : string.Empty;
            if (paramValue == compareTo)
            {
                result = true;
            }
            return result;
        }

        

    }
}
