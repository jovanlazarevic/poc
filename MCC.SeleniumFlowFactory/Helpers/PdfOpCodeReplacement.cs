﻿using PdfSharp.Pdf.Content.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class PdfOpCodeReplacement
    {
		public PdfOpCodeReplacement(OpCodeReplacement opCode, string substitution)
        {
            OpCode = opCode;
            Substitution = substitution;
        }

        public OpCodeReplacement OpCode { get; set; }
        public string Substitution { get; set; }
    }

    /// <summary>
    /// Represents a PDF content stream operator description.
    /// taken from https://github.com/empira/PDFsharp/blob/master/src/PdfSharp/Pdf.Content.Objects/Operators.cs
    /// </summary>
    public sealed class OpCodeReplacement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpCodeReplacement"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="OpCodeName">The enum value of the operator.</param>
        /// <param name="operands">The number of operands.</param>
        /// <param name="postscript">The postscript equivalent, or null, if no such operation exists.</param>
        /// <param name="flags">The flags.</param>
        /// <param name="description">The description from Adobe PDF Reference.</param>
        internal OpCodeReplacement(string name, OpCodeName opCodeName, int operands, string postscript, OpCodeFlags flags, string description)
        {
            Name = name;
            OpCodeName = opCodeName;
            Operands = operands;
            Postscript = postscript;
            Flags = flags;
            Description = description;
        }

        /// <summary>
        /// The name of the operator.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// The enum value of the operator.
        /// </summary>
        public readonly OpCodeName OpCodeName;

        /// <summary>
        /// The number of operands. -1 indicates a variable number of operands.
        /// </summary>
        public readonly int Operands;

        /// <summary>
        /// The flags.
        /// </summary>
        public readonly OpCodeFlags Flags;

        /// <summary>
        /// The postscript equivalent, or null, if no such operation exists.
        /// </summary>
        public readonly string Postscript;

        /// <summary>
        /// The description from Adobe PDF Reference.
        /// </summary>
        public readonly string Description;

    }


    /// <summary>
    /// Static class with all PDF op-codes taken from https://github.com/empira/PDFsharp/blob/master/src/PdfSharp/Pdf.Content.Objects/Operators.cs
    /// </summary>
    public static class OpCodes
    {
        public static readonly OpCodeReplacement b = new OpCodeReplacement("b", OpCodeName.b, 0, "closepath, fill, stroke", OpCodeFlags.None,
            "Close, fill, and stroke path using nonzero winding number");

        public static readonly OpCodeReplacement B = new OpCodeReplacement("B", OpCodeName.B, 0, "fill, stroke", OpCodeFlags.None,
            "Fill and stroke path using nonzero winding number rule");

        public static readonly OpCodeReplacement bx = new OpCodeReplacement("b*", OpCodeName.bx, 0, "closepath, eofill, stroke", OpCodeFlags.None,
            "Close, fill, and stroke path using even-odd rule");

        public static readonly OpCodeReplacement Bx = new OpCodeReplacement("B*", OpCodeName.Bx, 0, "eofill, stroke", OpCodeFlags.None,
            "Fill and stroke path using even-odd rule");

        public static readonly OpCodeReplacement BDC = new OpCodeReplacement("BDC", OpCodeName.BDC, -1, null, OpCodeFlags.None,
            "(PDF 1.2) Begin marked-content sequence with property list");

        public static readonly OpCodeReplacement BI = new OpCodeReplacement("BI", OpCodeName.BI, 0, null, OpCodeFlags.None,
            "Begin inline image object");

        public static readonly OpCodeReplacement BMC = new OpCodeReplacement("BMC", OpCodeName.BMC, 1, null, OpCodeFlags.None,
            "(PDF 1.2) Begin marked-content sequence");

        public static readonly OpCodeReplacement BT = new OpCodeReplacement("BT", OpCodeName.BT, 0, null, OpCodeFlags.None,
            "Begin text object");

        public static readonly OpCodeReplacement BX = new OpCodeReplacement("BX", OpCodeName.BX, 0, null, OpCodeFlags.None,
            "(PDF 1.1) Begin compatibility section");

        public static readonly OpCodeReplacement c = new OpCodeReplacement("c", OpCodeName.c, 6, "curveto", OpCodeFlags.None,
            "Append curved segment to path (three control points)");

        public static readonly OpCodeReplacement cm = new OpCodeReplacement("cm", OpCodeName.cm, 6, "concat", OpCodeFlags.None,
            "Concatenate matrix to current transformation matrix");

        public static readonly OpCodeReplacement CS = new OpCodeReplacement("CS", OpCodeName.CS, 1, "setcolorspace", OpCodeFlags.None,
            "(PDF 1.1) Set color space for stroking operations");

        public static readonly OpCodeReplacement cs = new OpCodeReplacement("cs", OpCodeName.cs, 1, "setcolorspace", OpCodeFlags.None,
            "(PDF 1.1) Set color space for nonstroking operations");

        public static readonly OpCodeReplacement d = new OpCodeReplacement("d", OpCodeName.d, 2, "setdash", OpCodeFlags.None,
            "Set line dash pattern");

        public static readonly OpCodeReplacement d0 = new OpCodeReplacement("d0", OpCodeName.d0, 2, "setcharwidth", OpCodeFlags.None,
            "Set glyph width in Type 3 font");

        public static readonly OpCodeReplacement d1 = new OpCodeReplacement("d1", OpCodeName.d1, 6, "setcachedevice", OpCodeFlags.None,
            "Set glyph width and bounding box in Type 3 font");

        public static readonly OpCodeReplacement Do = new OpCodeReplacement("Do", OpCodeName.Do, 1, null, OpCodeFlags.None,
            "Invoke named XObject");

        public static readonly OpCodeReplacement DP = new OpCodeReplacement("DP", OpCodeName.DP, 2, null, OpCodeFlags.None,
            "(PDF 1.2) Define marked-content point with property list");

        public static readonly OpCodeReplacement EI = new OpCodeReplacement("EI", OpCodeName.EI, 0, null, OpCodeFlags.None,
            "End inline image object");

        public static readonly OpCodeReplacement EMC = new OpCodeReplacement("EMC", OpCodeName.EMC, 0, null, OpCodeFlags.None,
            "(PDF 1.2) End marked-content sequence");

        public static readonly OpCodeReplacement ET = new OpCodeReplacement("ET", OpCodeName.ET, 0, null, OpCodeFlags.None,
            "End text object");

        public static readonly OpCodeReplacement EX = new OpCodeReplacement("EX", OpCodeName.EX, 0, null, OpCodeFlags.None,
            "(PDF 1.1) End compatibility section");

        public static readonly OpCodeReplacement f = new OpCodeReplacement("f", OpCodeName.f, 0, "fill", OpCodeFlags.None,
            "Fill path using nonzero winding number rule");

        public static readonly OpCodeReplacement F = new OpCodeReplacement("F", OpCodeName.F, 0, "fill", OpCodeFlags.None,
            "Fill path using nonzero winding number rule (obsolete)");

        public static readonly OpCodeReplacement fx = new OpCodeReplacement("f*", OpCodeName.fx, 0, "eofill", OpCodeFlags.None,
            "Fill path using even-odd rule");

        public static readonly OpCodeReplacement G = new OpCodeReplacement("G", OpCodeName.G, 1, "setgray", OpCodeFlags.None,
            "Set gray level for stroking operations");

        public static readonly OpCodeReplacement g = new OpCodeReplacement("g", OpCodeName.g, 1, "setgray", OpCodeFlags.None,
            "Set gray level for nonstroking operations");

        public static readonly OpCodeReplacement gs = new OpCodeReplacement("gs", OpCodeName.gs, 1, null, OpCodeFlags.None,
            "(PDF 1.2) Set parameters from graphics state parameter dictionary");

        public static readonly OpCodeReplacement h = new OpCodeReplacement("h", OpCodeName.h, 0, "closepath", OpCodeFlags.None,
            "Close subpath");

        public static readonly OpCodeReplacement i = new OpCodeReplacement("i", OpCodeName.i, 1, "setflat", OpCodeFlags.None,
            "Set flatness tolerance");

        public static readonly OpCodeReplacement ID = new OpCodeReplacement("ID", OpCodeName.ID, 0, null, OpCodeFlags.None,
            "Begin inline image data");

        public static readonly OpCodeReplacement j = new OpCodeReplacement("j", OpCodeName.j, 1, "setlinejoin", OpCodeFlags.None,
            "Set line join style");

        public static readonly OpCodeReplacement J = new OpCodeReplacement("J", OpCodeName.J, 1, "setlinecap", OpCodeFlags.None,
            "Set line cap style");

        public static readonly OpCodeReplacement K = new OpCodeReplacement("K", OpCodeName.K, 4, "setcmykcolor", OpCodeFlags.None,
            "Set CMYK color for stroking operations");

        public static readonly OpCodeReplacement k = new OpCodeReplacement("k", OpCodeName.k, 4, "setcmykcolor", OpCodeFlags.None,
            "Set CMYK color for nonstroking operations");

        public static readonly OpCodeReplacement l = new OpCodeReplacement("l", OpCodeName.l, 2, "lineto", OpCodeFlags.None,
            "Append straight line segment to path");

        public static readonly OpCodeReplacement m = new OpCodeReplacement("m", OpCodeName.m, 2, "moveto", OpCodeFlags.None,
            "Begin new subpath");

        public static readonly OpCodeReplacement M = new OpCodeReplacement("M", OpCodeName.M, 1, "setmiterlimit", OpCodeFlags.None,
            "Set miter limit");

        public static readonly OpCodeReplacement MP = new OpCodeReplacement("MP", OpCodeName.MP, 1, null, OpCodeFlags.None,
            "(PDF 1.2) Define marked-content point");

        public static readonly OpCodeReplacement n = new OpCodeReplacement("n", OpCodeName.n, 0, null, OpCodeFlags.None,
            "End path without filling or stroking");

        public static readonly OpCodeReplacement q = new OpCodeReplacement("q", OpCodeName.q, 0, "gsave", OpCodeFlags.None,
            "Save graphics state");

        public static readonly OpCodeReplacement Q = new OpCodeReplacement("Q", OpCodeName.Q, 0, "grestore", OpCodeFlags.None,
            "Restore graphics state");

        public static readonly OpCodeReplacement re = new OpCodeReplacement("re", OpCodeName.re, 4, null, OpCodeFlags.None,
            "Append rectangle to path");

        public static readonly OpCodeReplacement RG = new OpCodeReplacement("RG", OpCodeName.RG, 3, "setrgbcolor", OpCodeFlags.None,
            "Set RGB color for stroking operations");

        public static readonly OpCodeReplacement rg = new OpCodeReplacement("rg", OpCodeName.rg, 3, "setrgbcolor", OpCodeFlags.None,
            "Set RGB color for nonstroking operations");

        public static readonly OpCodeReplacement ri = new OpCodeReplacement("ri", OpCodeName.ri, 1, null, OpCodeFlags.None,
            "Set color rendering intent");

        public static readonly OpCodeReplacement s = new OpCodeReplacement("s", OpCodeName.s, 0, "closepath,stroke", OpCodeFlags.None,
            "Close and stroke path");

        public static readonly OpCodeReplacement S = new OpCodeReplacement("S", OpCodeName.S, 0, "stroke", OpCodeFlags.None,
            "Stroke path");

        public static readonly OpCodeReplacement SC = new OpCodeReplacement("SC", OpCodeName.SC, -1, "setcolor", OpCodeFlags.None,
            "(PDF 1.1) Set color for stroking operations");

        public static readonly OpCodeReplacement sc = new OpCodeReplacement("sc", OpCodeName.sc, -1, "setcolor", OpCodeFlags.None,
            "(PDF 1.1) Set color for nonstroking operations");

        public static readonly OpCodeReplacement SCN = new OpCodeReplacement("SCN", OpCodeName.SCN, -1, "setcolor", OpCodeFlags.None,
            "(PDF 1.2) Set color for stroking operations (ICCBased and special color spaces)");

        public static readonly OpCodeReplacement scn = new OpCodeReplacement("scn", OpCodeName.scn, -1, "setcolor", OpCodeFlags.None,
            "(PDF 1.2) Set color for nonstroking operations (ICCBased and special color spaces)");

        public static readonly OpCodeReplacement sh = new OpCodeReplacement("sh", OpCodeName.sh, 1, "shfill", OpCodeFlags.None,
            "(PDF 1.3) Paint area defined by shading pattern");

        public static readonly OpCodeReplacement Tx = new OpCodeReplacement("T*", OpCodeName.Tx, 0, null, OpCodeFlags.None,
            "Move to start of next text line");

        public static readonly OpCodeReplacement Tc = new OpCodeReplacement("Tc", OpCodeName.Tc, 1, null, OpCodeFlags.None,
            "Set character spacing");

        public static readonly OpCodeReplacement Td = new OpCodeReplacement("Td", OpCodeName.Td, 2, null, OpCodeFlags.None,
            "Move text position");

        public static readonly OpCodeReplacement TD = new OpCodeReplacement("TD", OpCodeName.TD, 2, null, OpCodeFlags.None,
            "Move text position and set leading");

        public static readonly OpCodeReplacement Tf = new OpCodeReplacement("Tf", OpCodeName.Tf, 2, "selectfont", OpCodeFlags.None,
            "Set text font and size");

        public static readonly OpCodeReplacement Tj = new OpCodeReplacement("Tj", OpCodeName.Tj, 1, "show", OpCodeFlags.TextOut,
            "Show text");

        public static readonly OpCodeReplacement TJ = new OpCodeReplacement("TJ", OpCodeName.TJ, 1, null, OpCodeFlags.TextOut,
            "Show text, allowing individual glyph positioning");

        public static readonly OpCodeReplacement TL = new OpCodeReplacement("TL", OpCodeName.TL, 1, null, OpCodeFlags.None,
            "Set text leading");

        public static readonly OpCodeReplacement Tm = new OpCodeReplacement("Tm", OpCodeName.Tm, 6, null, OpCodeFlags.None,
            "Set text matrix and text line matrix");

        public static readonly OpCodeReplacement Tr = new OpCodeReplacement("Tr", OpCodeName.Tr, 1, null, OpCodeFlags.None,
            "Set text rendering mode");

        public static readonly OpCodeReplacement Ts = new OpCodeReplacement("Ts", OpCodeName.Ts, 1, null, OpCodeFlags.None,
            "Set text rise");

        public static readonly OpCodeReplacement Tw = new OpCodeReplacement("Tw", OpCodeName.Tw, 1, null, OpCodeFlags.None,
            "Set word spacing");

        public static readonly OpCodeReplacement Tz = new OpCodeReplacement("Tz", OpCodeName.Tz, 1, null, OpCodeFlags.None,
            "Set horizontal text scaling");

        public static readonly OpCodeReplacement v = new OpCodeReplacement("v", OpCodeName.v, 4, "curveto", OpCodeFlags.None,
            "Append curved segment to path (initial point replicated)");

        public static readonly OpCodeReplacement w = new OpCodeReplacement("w", OpCodeName.w, 1, "setlinewidth", OpCodeFlags.None,
            "Set line width");

        public static readonly OpCodeReplacement W = new OpCodeReplacement("W", OpCodeName.W, 0, "clip", OpCodeFlags.None,
            "Set clipping path using nonzero winding number rule");

        public static readonly OpCodeReplacement Wx = new OpCodeReplacement("W*", OpCodeName.Wx, 0, "eoclip", OpCodeFlags.None,
            "Set clipping path using even-odd rule");

        public static readonly OpCodeReplacement y = new OpCodeReplacement("y", OpCodeName.y, 4, "curveto", OpCodeFlags.None,
            "Append curved segment to path (final point replicated)");

        public static readonly OpCodeReplacement QuoteSingle = new OpCodeReplacement("'", OpCodeName.QuoteSingle, 1, null, OpCodeFlags.TextOut,
            "Move to next line and show text");

        public static readonly OpCodeReplacement QuoteDbl = new OpCodeReplacement("\"", OpCodeName.QuoteDbl, 3, null, OpCodeFlags.TextOut,
            "Set word and character spacing, move to next line, and show text");

    }

}
