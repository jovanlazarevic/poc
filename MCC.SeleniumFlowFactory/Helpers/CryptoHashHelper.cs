﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MCC.SeleniumFlowFactory.Helpers
{
    public class CryptoHashHelper
    {
        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            return FormatHashedBytes(hash);
        }

        public static string GetFileHashSha256(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                var sha = new SHA256Managed();
                byte[] hash = sha.ComputeHash(stream);
                return FormatHashedBytes(hash);
            }
        }

        public static string GetBytesSha256(byte[] bytes)
        {
            var sha = new SHA256Managed();
            byte[] hash = sha.ComputeHash(bytes);
            return FormatHashedBytes(hash);
        }

        private static string FormatHashedBytes(byte[] bytes)
        {
            string hashString = string.Empty;
            foreach (var x in bytes)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}
