﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MCC.SeleniumFlowFactory.Helpers
{
    public static class StepParamsExtractor
    {
        public static List<string> GetAvailableParams(string stepTypeName)
        {
            List<string> result = null;
            try
            {
                var assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                var instance = Assembly.GetExecutingAssembly().CreateInstance($"{assemblyName}.{stepTypeName}");
                result = ((Step)instance).GetAvailableParams();
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static List<Tuple<string, List<string>>> GetAvailableSteps()
        {
            var result = new List<Tuple<string, List<string>>>();
            try
            {
                var assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                var c = new Constants();
                foreach (var stepType in typeof(Constants.StepType).GetFields())
                {
                    var instance = Assembly.GetExecutingAssembly().CreateInstance($"{assemblyName}.{stepType.Name}");
                    var stepParams = ((Step)instance).GetAvailableParams();
                    result.Add(new Tuple<string, List<string>>(stepType.Name, stepParams));
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
