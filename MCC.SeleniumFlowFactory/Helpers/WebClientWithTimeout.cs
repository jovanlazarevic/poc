﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MCC.SeleniumFlowFactory.Helpers
{
    public class WebClientWithTimeout : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest wr = base.GetWebRequest(address);
            wr.Timeout = 59000; // timeout in milliseconds (ms)
            return wr;
        }
    }
}
