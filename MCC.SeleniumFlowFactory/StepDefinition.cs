﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class StepDefinition
    {
        public StepDefinition(string typeName, string comment = null, int typeId = 0)
        {
            TypeName = typeName;
            TypeId = typeId;
            Params = new List<KeyValuePair<string, string>>();
            ParentDefinition = null;
            Comment = comment;
        }
        public decimal StepNumber { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public List<KeyValuePair<string, string>> Params { get; set; }
        public StepDefinition ParentDefinition { get; set; }
        public string Comment { get; set; }
    }
}
