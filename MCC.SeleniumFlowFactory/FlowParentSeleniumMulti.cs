﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FlowParentSeleniumMulti : FlowParentSelenium
    {
        List<string> RootUrls { get; set; }

        public bool PrepareAndCheck(FlowStateSeleniumMulti state)
        {
            RootUrls = state.RootUrls;

            return base.PrepareAndCheck(state);
        }


        public new void ProcessWork()
        {
            if (RootUrls != null && RootUrls.Count > 0)
            {
                var percentsStep = (Constants.FinishedPercents.RangeTo - Constants.FinishedPercents.RangeFrom) / RootUrls.Count;
                foreach (var url in RootUrls)
                {
                    _flowState.RootUrl = url;
                    _flowState.Steps = new List<Step>();

                    // RUN
                    Step step = new FirstStep();
                    ProcessSteps(step, _flowState);

                    _flowState.FinishedPercents.IncrementPercents(percentsStep);
                }
            }
            _flowState.DisposeDrivers();
        }


    }
}
