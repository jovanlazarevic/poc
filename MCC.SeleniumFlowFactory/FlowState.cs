﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public abstract class FlowState
    {
        #region Properties
        public List<Step> Steps { get; set; }
        public List<StepDefinition> StepDefinitions { get; set; }
        public List<KeyValuePair<DateTime, object>> Exceptions { get; set; }
        public List<KeyValuePair<string, object>> Results { get; set; }
        public FinishedPercents FinishedPercents { get; set; }
        /// <summary>
        ///  Only for debug purpose
        /// </summary>
        public List<ExecutionDebugStep> ExecutionDebugSteps { get; set; }
        public bool IsDebug { get; set; }


        #endregion



        public FlowState(List<StepDefinition> stepDefinitions)
        {
            this.Steps = new List<Step>();
            this.StepDefinitions = stepDefinitions;
            this.Exceptions = new List<KeyValuePair<DateTime, object>>();
            this.Results = new List<KeyValuePair<string, object>>();
            FinishedPercents = new FinishedPercents();
            ExecutionDebugSteps = new List<ExecutionDebugStep>();
        }

        public void AddException(Exception ex)
        {
            this.Exceptions.Add(new KeyValuePair<DateTime, object>(DateTime.UtcNow, ex));
            Console.WriteLine(ex.ToString());
            // Add to debug steps
            if (IsDebug)
            {
                var exInfo = new List<StepParamInfo>() { new StepParamInfo("Exception", ex.Message, ex.ToString(), this.FinishedPercents.Percents) };
                this.ExecutionDebugSteps.Add(new ExecutionDebugStep("Exception", exInfo, null, false));
            }
        }

        public void AddDebugStep(Step step)
        {
            // Params / results
            try
            {
                var stepParams = new List<StepParamInfo>();
                if (step.StepDefinition != null && step?.StepDefinition.Params != null)
                {
                    foreach (var p in step.StepDefinition.Params.Where(x => x.Key != Constants.ParamType.AddToResults))
                    {
                        var strVal = string.Empty;
                        if (!string.IsNullOrEmpty(p.Value))
                        {
                            foreach (var pp in p.Value.Split(','))
                            {
                                var ppStrVal = string.Empty;
                                var objVal = step.GetResult(pp);
                                if (objVal == null)
                                {
                                    ppStrVal = "null";
                                }
                                else if (objVal != null && objVal is IWebElement)
                                {
                                    try
                                    {
                                        var tagName = ((IWebElement)objVal).TagName;
                                        var id = !string.IsNullOrEmpty(((IWebElement)objVal).GetAttribute("id")) ? $" id='{((IWebElement)objVal).GetAttribute("id")}'" : string.Empty;
                                        var name = !string.IsNullOrEmpty(((IWebElement)objVal).GetAttribute("name")) ? $" name='{((IWebElement)objVal).GetAttribute("name")}'" : string.Empty;
                                        var href = !string.IsNullOrEmpty(((IWebElement)objVal).GetAttribute("href")) ? $" href='{((IWebElement)objVal).GetAttribute("href")}'" : string.Empty;
                                        ppStrVal = $"{objVal.ToString()} (<{tagName}{id}{name}{href}>)";
                                    }
                                    catch (Exception ex)
                                    {
                                        ppStrVal = objVal.ToString();
                                    }
                                }
                                else
                                {
                                    ppStrVal = objVal.ToString();
                                }
                                strVal = string.IsNullOrEmpty(strVal) ? $"'{ppStrVal}'" : $"{strVal},'{ppStrVal}'";
                            }
                        }
                        else
                        {
                            strVal = "null";
                        }
                        stepParams.Add(new StepParamInfo(p.Key, p.Value, strVal, this.FinishedPercents.Percents));

                    }
                    stepParams.Add(new StepParamInfo("Result", "", step.Result?.ToString(), this.FinishedPercents.Percents));
                }

                this.ExecutionDebugSteps.Add(new ExecutionDebugStep(step.StepName, stepParams, step.StepDefinition?.Comment, true));
            }
            catch (Exception ex)
            {
                var exInfo = new List<StepParamInfo>() { new StepParamInfo("Exception during storing debug info", ex.Message, ex.ToString(), this.FinishedPercents.Percents) };
                this.ExecutionDebugSteps.Add(new ExecutionDebugStep("Exception", exInfo, step?.StepDefinition?.Comment, false));
            }
        }
    }
}
