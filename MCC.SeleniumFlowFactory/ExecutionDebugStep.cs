﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    /// <summary>
    /// For debug / review purpose
    /// </summary>
    public class ExecutionDebugStep
    {
        public DateTime DateTime { get; set; }
        public string StepName { get; set; }
        public List<StepParamInfo> Variables { get; set; }
        public string StepComment { get; set; }
        public bool IsSuccessful { get; set; }

        public ExecutionDebugStep(string stepTypeName, List<StepParamInfo> variables, string stepComment, bool isSuccessful)
        {
            DateTime = DateTime.UtcNow;
            StepName = stepTypeName;
            Variables = variables;
            StepComment = stepComment;
            IsSuccessful = isSuccessful;
        }
    }

    public class StepParamInfo
    {
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
        public string ObjectValue { get; set; }
        public decimal FinishedPercents { get; set; }

        public StepParamInfo(string paramName, string paramValue, string objectValue, decimal finishedPercents)
        {
            ParamName = paramName;
            ParamValue = paramValue;
            ObjectValue = objectValue;
            FinishedPercents = finishedPercents;
        }
    }
}
