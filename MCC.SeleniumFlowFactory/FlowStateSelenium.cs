﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FlowStateSelenium : FlowState
    {
        public string DefinitionName { get; set; }
        public string DefinitionUrl { get; set; }
        public string DefinitionType { get; set; }

        public int MaxLoopCount { get; set; }
        public string RootUrl { get; set; }
        internal RemoteWebDriver TemporaryDriver { get; set; }
        internal ChromeDriver StreamDriver { get; set; }

        internal Dictionary<string, RemoteWebDriver> Drivers { get; set; }
        internal string SelectedDriver { get; set; }
        internal string TempDownloadFilePath { get; set; }
        private bool UseChromeDriver { get; set; }

        public FlowStateSelenium(string definitionName, string definitionUrl, string definitionType,
                                 List<StepDefinition> stepDefinitions, string rootUrl, int maxLoopCount = 100, bool isDebug = false, string tempDownloadFilePath = "C:\\temp", bool useChromeDriver = false)
            : base(stepDefinitions)
        {
            DefinitionName = definitionName;
            DefinitionUrl = definitionUrl;
            DefinitionType = definitionType;

            MaxLoopCount = maxLoopCount;
            RootUrl = rootUrl;
            IsDebug = isDebug;
            UseChromeDriver = useChromeDriver;
            //Drivers
            SelectedDriver = "0";
            Drivers = new Dictionary<string, RemoteWebDriver>();
            // Temporary driver 
            if (!UseChromeDriver)
            {
                PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService(AppDomain.CurrentDomain.BaseDirectory);
                service.HideCommandPromptWindow = true;
                this.TemporaryDriver = new PhantomJSDriver(service);
            }
            else
            {
                var tmpOptions = new ChromeOptions();
                tmpOptions.AddArguments(new List<string>() {"--silent-launch",
                                                            "--no-startup-window",
                                                            "no-sandbox",
                                                            "headless"});
                this.TemporaryDriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, tmpOptions);
            }
            this.StreamDriver = null;
            TempDownloadFilePath = tempDownloadFilePath;
        }

        internal RemoteWebDriver CurrentDriver()
        {
            RemoteWebDriver driver = null;
            if (SelectedDriver == "TemporaryDriver")
            {
                driver = this.TemporaryDriver;
            }
            else if (!Drivers.TryGetValue(SelectedDriver, out driver))
            {
                if (!UseChromeDriver)
                {
                    PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService(AppDomain.CurrentDomain.BaseDirectory);
                    service.HideCommandPromptWindow = true;
                    service.IgnoreSslErrors = true;
                    service.LoadImages = false;
                    /* tried for download
                     * PhantomJSOptions options = new PhantomJSOptions();
                    options.UnhandledPromptBehavior = UnhandledPromptBehavior.Accept;
                    options.PageLoadStrategy = PageLoadStrategy.Normal;
                    options.AddAdditionalCapability("browser.helperApps.neverAsk.saveToDisk", "application/pdf,application/octet-stream");
                    options.AddAdditionalCapability("browser.helperApps.alwaysAsk.force", false);
                    options.AddAdditionalCapability("browser.download.dir", @"C:\Users\jovan\Downloads");
                    options.AddAdditionalCapability("browser.download.folderList", 2);
                    options.AddAdditionalCapability("browser.helperApps.neverAsk.openFile", "application/csv,application/excel,application/vnd.msexcel,application/vnd.ms-excel,text/anytext,text/comma-separated-values,text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/octet-stream");
                    options.AddAdditionalCapability("pdfjs.disabled", true);
                    driver = new PhantomJSDriver(service, options);*/

                    //PhantomJSOptions options = new PhantomJSOptions();
                    //options.AddAdditionalCapability("browser.helperApps.alwaysAsk.force", false);
                    driver = new PhantomJSDriver(service);

                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 59);
                    Drivers.TryAdd(SelectedDriver, driver);
                }
                else
                {
                    var options = new ChromeOptions();
                    // Headless browser
                    options.AddArguments(new List<string>() { "--silent-launch",
                                                                "--no-startup-window",
                                                                "no-sandbox",
                                                                "headless" });
                    // Visible browser
                    //options.AddArguments(new List<string>() { "no-sandbox" });
                    driver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, options);
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 59);
                    Drivers.TryAdd(SelectedDriver, driver);
                }
            }

            return driver;
        }

        internal ChromeDriver GetStreamDriver()
        {
            if (this.StreamDriver == null)
            {
                // For downloading file, Doesn't work in headless mode. TODO: test on server
                var chromeOptions = new ChromeOptions();
                // https://github.com/xolvio/chimp/issues/679   comments
                chromeOptions.AddUserProfilePreference("download.default_directory", TempDownloadFilePath);
                chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
                chromeOptions.AddUserProfilePreference("download.directory_upgrade", true);
                //chromeOptions.AddArguments(new List<string>() {"--disable-gpu",
                //                                               "--window-size=1050,900",
                //                                               "no-sandbox",
                //                                               "headless" 
                //});
                this.StreamDriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, chromeOptions);
            }
            return this.StreamDriver;
        }

        internal void DisposeDrivers()
        {
            try
            {
                this.TemporaryDriver.Dispose();
                foreach (var driver in Drivers.Values)
                {
                    driver.Quit();
                }
                this.Drivers.Clear();
                if (this.StreamDriver != null)
                {
                    this.StreamDriver.Quit();
                }
            }
            finally
            {
            }
        }

    }
}
