﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class Constants
    {
        #region Step implementations

        public static class StepType
        {
            public const string ForEach = "ForEach";
            public const string ForEachEnd = "ForEachEnd";
            public const string While = "While";
            public const string WhileEnd = "WhileEnd";
            public const string If = "If";
            public const string EndIf = "EndIf";
            public const string GetElementByXPath = "GetElementByXPath";
            public const string GetElementsByXPath = "GetElementsByXPath";
            public const string GetLastElementByXPath = "GetLastElementByXPath";
            public const string GetElementByTagName = "GetElementByTagName";
            public const string GetElementById = "GetElementById";
            public const string GetElementText = "GetElementText";
            public const string GetElementInnerHtml = "GetElementInnerHtml";
            public const string GetPdfStream = "GetPdfStream";
            public const string GetPdfText = "GetPdfText";
            public const string GetElementAttribute = "GetElementAttribute";
            public const string SetElementAttribute = "SetElementAttribute";
            public const string GoToUrl = "GoToUrl";
            public const string NavigateBack = "NavigateBack";
            public const string SwitchToFrame = "SwitchToFrame";
            public const string SwitchToWindow = "SwitchToWindow";
            public const string Click = "Click";
            public const string MakeResultObject = "MakeResultObject";
            public const string GetElementHash = "GetElementHash";
            public const string GetStringHash = "GetStringHash";
            public const string FormatString = "FormatString";
            public const string StringReplace = "StringReplace";
            public const string WaitUntilElementAttributeNotContains = "WaitUntilElementAttributeNotContains";
            public const string WaitUntilElementAttributeNotContainsOrEmpty = "WaitUntilElementAttributeNotContainsOrEmpty";
            public const string WaitUntilElementIsVisible = "WaitUntilElementIsVisible";
            public const string NewTab = "NewTab";
            public const string SwitchToNewTab = "SwitchToNewTab";
            public const string CloseTab = "CloseTab";
            /// <summary>
            /// Returns page inner html with using secondary driver
            /// </summary>
            public const string GetUrlContent = "GetUrlContent";
            /// <summary>
            /// Returns page inner html hash with using secondary driver
            /// </summary>
            public const string GetUrlContentHash = "GetUrlContentHash";
            public const string GetUrlInnerHtmlHash = "GetUrlInnerHtmlHash";
            public const string Wait = "Wait";
            public const string SwitchDriver = "SwitchDriver";
            public const string SaveVariable = "SaveVariable";
        }

        #endregion

        #region Param types

        public static class ParamType
        {
            public const string Url = "Url";
            public const string ByXPath = "ByXPath";
            /// <summary>
            /// If element(s) not found by XPath, another try with Alternate XPath will be executed
            /// </summary>
            public const string ByAlternateXPath = "ByAlternateXPath"; 
            public const string ByTagName = "ByTagName";
            public const string ById = "ById";
            public const string PrevText = "PrevText";
            public const string InputObject = "InputObject";
            public const string InputValue = "InputValue";
            public const string InputResults = "InputResults";
            public const string DriverId = "DriverId";
            /// <summary>
            /// Save step result as global variable
            /// </summary>
            public const string AddOrUpdateResult = "AddOrUpdateResult";
            /// <summary>
            /// Save step result in list of results
            /// </summary>
            public const string AddToResults = "AddToResults";
            /// <summary>
            /// When few variables added to results, their keys could be changed - to be unified for later use
            /// </summary>
            public const string AddToResultsReplaceKeys = "AddToResultsReplaceKeys";
            public const string ElementAttribute = "ElementAttribute";
            public const string ElementAttributeContains = "ElementAttributeContains";
            public const string WaitSeconds = "WaitSeconds";
            public const string SwitchTo = "SwitchTo";
            public const string StringFormat = "StringFormat";
            public const string FindString = "FindString";
            public const string ReplaceString = "ReplaceString";

            public const string EqualsTo = "EqualsTo";
            public const string NotEqualsTo= "NotEqualsTo";
            public const string ContainsIn = "ContainsIn";
            public const string EndsWith = "EndsWith";
            public const string NotEndsWith = "NotEndsWith";
            public const string EqualsToObject = "EqualsToObject";
            public const string NotEqualsToObject = "NotEqualsToObject";
            public const string NotContainsInResultsKeys = "NotContainsInResultsKeys";
            public const string IsVisible = "IsVisible";
            public const string IsNotVisible = "IsNotVisible";
        }
        #endregion

        #region Pdf

        public static readonly List<PdfOpCodeReplacement> Replacements =
                    new List<PdfOpCodeReplacement>
                    {
                                new PdfOpCodeReplacement(OpCodes.QuoteSingle, Environment.NewLine),
                                new PdfOpCodeReplacement(OpCodes.QuoteDbl, Environment.NewLine),
                                new PdfOpCodeReplacement(OpCodes.TD, Environment.NewLine),
                                new PdfOpCodeReplacement(OpCodes.BDC, Environment.NewLine),
                                new PdfOpCodeReplacement(OpCodes.BMC, Environment.NewLine),
                                new PdfOpCodeReplacement(OpCodes.Tc, " ")
                    };


        #endregion

        #region Percents

        public static class FinishedPercents
        {
            public const decimal RangeFrom = 2.0M;
            public const decimal RangeTo = 80.0M;
            public const decimal MinStepSize = 0.001M;
        }

        #endregion
    }
}
