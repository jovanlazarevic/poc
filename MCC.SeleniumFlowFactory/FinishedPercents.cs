﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FinishedPercents
    {
        public decimal Percents { get; set; }

        public FinishedPercents()
        {
            Percents = Constants.FinishedPercents.RangeFrom;
        }

        public void IncrementPercents(decimal start, decimal increment, int stepCount)
        {
            var p = start + increment * stepCount;
            if (p > Percents && p < Constants.FinishedPercents.RangeTo)
            {
                Percents = p;
                Console.Write($"{Math.Round(Percents, 3)}%  ");
            }
        }

        public void IncrementPercents(decimal increment)
        {
            if ((Percents + increment) < Constants.FinishedPercents.RangeTo)
            {
                Percents += increment;
                Console.Write($"{Math.Round(Percents, 3)}%  ");
            }
        }
    }
}
