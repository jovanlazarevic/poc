﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public abstract class FlowParent
    {
        protected void ProcessSteps(Step step, FlowState state)
        {
           Boolean keepGoing = true;
           while(keepGoing)
           {
                state.Steps.Add(step);
                var tempStep = step.ExecuteProcessStep(state);
                if (state.IsDebug)
                {
                    state.AddDebugStep(step);
                }
                step = tempStep;

                if (step == null)
                    keepGoing = false;
           }
        }
    }
}
