﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FlowParentSelenium : FlowParent
    {
        protected FlowStateSelenium _flowState;
        public FlowStateSelenium FlowState {
            get
            {
                return _flowState;
            }
        }

        /// <summary>
        /// Method:
        /// - Sets StepNumbers in definitions if at least one is missing, 
        /// - Creates references to parent StepDefinitions and 
        /// - Validates if params attached to steps are 
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public virtual bool PrepareAndCheck(FlowStateSelenium state)
        {
            var result = true;
            _flowState = state;

            // Add step numbers if missing
            if (state.StepDefinitions.Any(x => x.StepNumber <= 0))
            {
                decimal stepNumber = 1.0M;
                state.StepDefinitions.ForEach(x => x.StepNumber = (stepNumber++));
            }

            //CreateTreeStructure();

            result = result && SyntaxChecker.ValidateSyntax(_flowState);

            return result;
        }

        public void ProcessWork()
        {
            // RUN
            Step step = new FirstStep();
            ProcessSteps(step, _flowState);

            _flowState.DisposeDrivers();
        }

        /*private void CreateTreeStructure()
        {
            if (_flowState.StepDefinitions.Count > 0)
            {
                foreach (var def in _flowState.StepDefinitions)
                {
                    int loopCount = 1;
                    int ifCount = 1;
                    int whileCount = 1;
                    foreach (var pdef in _flowState.StepDefinitions
                                           .Where(x => x != null && x.StepNumber < def.StepNumber)
                                           .OrderByDescending(x => x.StepNumber))
                    {
                        if (pdef.TypeName == Constants.StepType.ForEach)
                            loopCount++;
                        else if (pdef.TypeName == Constants.StepType.ForEachEnd)
                            loopCount--;

                        if (pdef.TypeName == Constants.StepType.If)
                            ifCount++;
                        else if (pdef.TypeName == Constants.StepType.EndIf)
                            ifCount--;

                        if (pdef.TypeName == Constants.StepType.While)
                            whileCount++;
                        else if (pdef.TypeName == Constants.StepType.WhileEnd)
                            whileCount--;

                        if (loopCount == 2 || ifCount == 2 || whileCount == 2)
                        {
                            def.ParentDefinition = pdef;
                            break;
                        }
                    }
                }
            }
        }*/


        public List<string> GetAvailableParams(string stepTypeName)
        {
            return StepParamsExtractor.GetAvailableParams(stepTypeName);
        }

        public List<Tuple<string, List<string>>> GetAvailableSteps()
        {
            return StepParamsExtractor.GetAvailableSteps();
        }

    }
}
