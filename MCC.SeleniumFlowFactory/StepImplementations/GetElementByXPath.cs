﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementByXPath : Step
    {
        public GetElementByXPath()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                            Constants.ParamType.ByXPath,
                                                            Constants.ParamType.ByAlternateXPath,
                                                            Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByXPath });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
            if (!string.IsNullOrEmpty(xPath.Value?.ToString()))
            {
                ((FlowStateSelenium)fs).CurrentDriver().Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 10);
                Result = GetXPathResult(input, fs, xPath.Value);
                if (Result == null)
                {
                    xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByAlternateXPath);
                    Result = GetXPathResult(input, fs, xPath.Value);
                }

                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception("GetElementByXPath: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }

        private IWebElement GetXPathResult(IWebElement input, FlowState fs, string xpath)
        {
            IWebElement result = null;
            if (!string.IsNullOrEmpty(xpath))
            {
                if (input != null)
                {
                    result = input.FindElements(By.XPath(xpath)).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                }
                else
                {
                    result = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                }
            }
            return result;
        }
    }
}
