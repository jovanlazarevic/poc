﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementsByXPath : Step
    {
        public GetElementsByXPath()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                            Constants.ParamType.ByXPath,
                                                            Constants.ParamType.ByAlternateXPath,
                                                            Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByXPath });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();

            var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
            if (!string.IsNullOrEmpty(xPath.Value?.ToString()))
            {
                ((FlowStateSelenium)fs).CurrentDriver().Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 10);
                Result = GetResultList(input, fs, xPath.Value);
                if (((List<IWebElement>)Result).Count == 0)
                {
                    xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByAlternateXPath);
                    if (!string.IsNullOrEmpty(xPath.Value))
                        Result = GetResultList(input, fs, xPath.Value);
                }
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }

        private List<IWebElement> GetResultList(IWebElement input, FlowState fs, string xpath)
        {
            List<IWebElement> resultList = null;
            if (!string.IsNullOrEmpty(xpath))
            {
                if (!string.IsNullOrEmpty(xpath) && input != null && input is IWebElement)
                {
                    resultList = input.FindElements(By.XPath(xpath)).ToList();
                }
                else
                {
                    resultList = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).ToList();
                }
            }
            return resultList;
        }

    }
}
