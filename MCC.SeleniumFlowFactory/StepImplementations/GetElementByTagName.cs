﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementByTagName : Step
    {
        public GetElementByTagName()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                          Constants.ParamType.ByTagName,
                                                          Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByTagName });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();

            var tagName = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByTagName);
            if (!string.IsNullOrEmpty(tagName.Value))
            {
                if (input != null)
                {
                    Result = input.FindElement(By.TagName(tagName.Value));
                }
                else
                {
                    Result = ((FlowStateSelenium)fs).CurrentDriver().FindElementByTagName(tagName.Value);
                }
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
