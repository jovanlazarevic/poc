﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class Wait : Step
    {
        public Wait()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.WaitSeconds });
        }

        public override Step ProcessStep(FlowState fs)
        {
            //Console.WriteLine($"{this.GetType().Name}");

            var waitParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.WaitSeconds);
            decimal waitSec = 5.0M;
            decimal.TryParse(waitParam.Value ?? "5.0", out waitSec);
            System.Threading.Thread.Sleep((int)(waitSec * 1000));

            return CreateNextStep(this, fs);
        }
    }
}
