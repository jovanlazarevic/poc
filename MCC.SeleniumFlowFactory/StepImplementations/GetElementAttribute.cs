﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementAttribute : Step
    {
        public GetElementAttribute()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                          Constants.ParamType.ElementAttribute,
                                                          Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ElementAttribute });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            var attrParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ElementAttribute);
            if (input != null && !string.IsNullOrEmpty(attrParam.Value))
            {
                if (!string.IsNullOrEmpty(attrParam.Key))
                {
                    Result = input.GetAttribute(attrParam.Value);
                }
                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
