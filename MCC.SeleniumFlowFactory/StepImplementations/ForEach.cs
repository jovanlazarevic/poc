﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class ForEach : Step
    {
        public int StepCount = -1;

        private List<IWebElement> resultList;
        private int loopMax = 0;
        public decimal FinishedPercentsStart { get; set; }
        public decimal FinishedPercentsStep { get; set; }

        public ForEach()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                          Constants.ParamType.ByXPath,
                                                          Constants.ParamType.ByAlternateXPath,
                                                          Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByXPath });
        }

        public override Step ProcessStep(FlowState fs)
        {
            StepCount++;

            if (StepCount == 0)
                Console.WriteLine($"{this.GetType().Name}");

            IWebElement input = GetInputWebElement();

            var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
            if (StepCount == 0)
            {
                ((FlowStateSelenium)fs).CurrentDriver().Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                resultList = GetResultList(input, fs, xPath.Value);
                if (resultList.Count == 0)
                {
                    xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByAlternateXPath);
                    if (!string.IsNullOrEmpty(xPath.Value))
                        resultList = GetResultList(input, fs, xPath.Value);
                }

                loopMax = ((List<IWebElement>)resultList).Count;
                // Finished percents initialization
                FinishedPercentsStart = fs.FinishedPercents.Percents;
                FinishedPercentsStep = calculateFinishedPercentsStep(fs, Math.Min(loopMax, ((FlowStateSelenium)fs).MaxLoopCount));
            }

            // Finished percents incrementation
            fs.FinishedPercents.IncrementPercents(FinishedPercentsStart, FinishedPercentsStep, StepCount);
            // Find next step
            if (!string.IsNullOrEmpty(xPath.Value) && StepCount < Math.Min(loopMax, ((FlowStateSelenium)fs).MaxLoopCount))
            {
                Result = resultList.GetRange(StepCount, 1).First();
                SaveInFlowStateResults();
                this.NextStep = CreateInstance(GetNextStepDefinition(this.StepDefinition), fs); // First Step after current step
            }
            else
            {
                int loopCount = 1;
                foreach (var def in fs.StepDefinitions.Where(x => x.StepNumber > this.StepDefinition.StepNumber).OrderBy(x => x.StepNumber))
                {
                    if (def.TypeName == Constants.StepType.ForEach)
                        loopCount++;
                    else if (def.TypeName == Constants.StepType.ForEachEnd)
                        loopCount--;

                    if (loopCount == 0)
                    {
                        this.NextStep = CreateInstance(GetNextStepDefinition(def), fs); // First step after ForEachEnd
                        this.StepCount = -1;
                        this.loopMax = 0;
                        this.resultList = null;
                        break;
                    }
                }
            }

            return this.NextStep;
        }

        private List<IWebElement> GetResultList(IWebElement input, FlowState fs, string xpath)
        {
            List<IWebElement> resultList = null;
            if (!string.IsNullOrEmpty(xpath))
            {
                if (!string.IsNullOrEmpty(xpath) && input != null && input is IWebElement)
                {
                    resultList = input.FindElements(By.XPath(xpath)).ToList();
                }
                else
                {
                    resultList = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).ToList();
                }
            }
            return resultList;
        }


        #region Percents calculation

        private decimal calculateFinishedPercentsStep(FlowState fs, int loopMax)
        {
            var range = Constants.FinishedPercents.RangeTo - Constants.FinishedPercents.RangeFrom;
            decimal result = range;
            try
            {
                var prevForEach = (ForEach)FindParentLoop(fs);
                var numberOfParallelLoops = NumberOfParallelLoops(fs, prevForEach != null ? prevForEach.StepDefinition.StepNumber : 0);

                if (loopMax <= 0)
                    loopMax = 1;
                if (prevForEach != null && (prevForEach.StepDefinition?.StepNumber ?? 0.0M) > 0.0M)
                {
                    result = prevForEach.FinishedPercentsStep / numberOfParallelLoops / loopMax;
                }
                else
                {
                    result = range / numberOfParallelLoops / loopMax;
                }
            }
            catch (Exception)
            {
                // will not break execution because of exception in percentage calculation
                result = 1.0M;
            }
            return result;
        }

        private Step FindParentLoop(FlowState fs)
        {
            Step result = null;
            StepDefinition prevLoopDef = null;
            int loopCount = 1;
            foreach (var def in fs.StepDefinitions
                                   .Where(x => x != null && x.StepNumber < (this.StepDefinition?.StepNumber ?? 0))
                                   .OrderByDescending(x => x.StepNumber))
            {
                if (def.TypeName == Constants.StepType.ForEach)
                    loopCount++;
                else if (def.TypeName == Constants.StepType.ForEachEnd)
                    loopCount--;

                if (loopCount == 2)
                {
                    prevLoopDef = def;
                    break;
                }
            }

            if (prevLoopDef != null)
            {
                result = fs.Steps.Last(x => x.StepDefinition.StepNumber == prevLoopDef.StepNumber);
            }

            return result;
        }

        /// <summary>
        /// Count number of loops in the first level below the parent loop (anyway of If-EndIf blocks in between)
        /// </summary>
        /// <param name="fs"></param>
        /// <param name="parentLoopStepNumber"></param>
        /// <returns></returns>
        private int NumberOfParallelLoops(FlowState fs, decimal parentLoopStepNumber)
        {
            int result = 0;
            int loopCount = 0;
            bool firstLoopStarted = false;
            foreach (var def in fs.StepDefinitions
                                   .Where(x => x != null && x.StepNumber > parentLoopStepNumber && (x.TypeName == Constants.StepType.ForEach || x.TypeName == Constants.StepType.ForEachEnd))
                                   .OrderBy(x => x.StepNumber))
            {
                if (def.TypeName == Constants.StepType.ForEach)
                {
                    firstLoopStarted = true;
                    loopCount++;
                }
                else if (def.TypeName == Constants.StepType.ForEachEnd)
                    loopCount--;

                if (firstLoopStarted && loopCount == 0)
                {
                    result++;
                }
            }

            return result;
        }

        #endregion
    }
}
