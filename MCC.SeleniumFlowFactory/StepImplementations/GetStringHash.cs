﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace MCC.SeleniumFlowFactory
{
    public class GetStringHash : Step
    {
        public GetStringHash()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            string input = GetInputString();
            if (input != null)
            {
                Result = CryptoHashHelper.GetHashSha256(input.Trim());

                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception("GetElementHash: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
