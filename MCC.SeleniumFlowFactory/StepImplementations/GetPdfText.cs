﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using PdfSharp.Pdf.Content;
using PdfSharp.Pdf.Content.Objects;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace MCC.SeleniumFlowFactory
{
    public class GetPdfText : Step
    {
        public GetPdfText()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            Result = null;
            Stream input = (Stream)GetInputObject();
            if (input != null && input is Stream)
            {
                using (var doc = PdfReader.Open(input))
                {
                    for (int pageIndex = 0; pageIndex < doc.PageCount; pageIndex++)
                    {
                        var content = ContentReader.ReadContent(doc.Pages[pageIndex]);
                        var pageText = ExtractText(content);
                        var page = string.Join(string.Empty, pageText.ToArray()); //TODO: Benchmark this line and try to optimize
                                                                                  // use regular expression to match only regular characters
                        page = Regex.Replace(page, @"[^0-9a-zA-Z.,-_:;\(\)@/ \r\n]+", string.Empty);
                        Result += page;
                    }
                }
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            // Save result
            SaveInFlowStateResults();

            return CreateNextStep(this, fs);
        }


        public IEnumerable<string> ExtractText(CObject cObject)
        {
            if (cObject is COperator)
            {
                var cOperator = cObject as COperator;
                if (cOperator.OpCode.Name == OpCodeName.Tj.ToString() || // Show text
                    cOperator.OpCode.Name == OpCodeName.TJ.ToString())   // Show text, allowing individual glyph positioning
                {
                    foreach (var cOperand in cOperator.Operands)
                        foreach (var txt in ExtractText(cOperand))
                            yield return txt;
                }
                else
                {
                    var replacement = Constants.Replacements.FirstOrDefault(x => x.OpCode.Name == cOperator.OpCode.Name);
                    if (replacement != null)
                    {
                        yield return replacement.Substitution;
                    }
                }
            }
            else if (cObject is CSequence)
            {
                var cSequence = cObject as CSequence;
                foreach (var element in cSequence)
                    foreach (var txt in ExtractText(element))
                        yield return txt;
            }
            else if (cObject is CString)
            {
                var cString = cObject as CString;
                yield return cString.Value;
            }
        }
    }
}
