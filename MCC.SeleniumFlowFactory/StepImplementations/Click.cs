﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class Click : Step
    {
        public Click()
        {
            availableParams.Add(Constants.ParamType.InputObject);
        }

        public override Step ProcessStep(FlowState fs)
        {
            Console.WriteLine($"{this.GetType().Name}");

            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                input.Click();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }

    }
}
