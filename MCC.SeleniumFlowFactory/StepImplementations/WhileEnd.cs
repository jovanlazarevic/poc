﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class WhileEnd : Step
    {
        public override Step ProcessStep(FlowState fs)
        {
            StepDefinition nextDef = this.StepDefinition;
            int loopCount = 1;
            foreach (var def in fs.StepDefinitions.Where(x => x.StepNumber < this.StepDefinition.StepNumber).OrderByDescending(x => x.StepNumber))
            {
                if (def.TypeName == Constants.StepType.While)
                    loopCount--;
                else if (def.TypeName == Constants.StepType.WhileEnd)
                    loopCount++;

                if (loopCount == 0)
                {
                    nextDef = def;
                    break;
                }
            }

            this.NextStep = CreateInstance(nextDef, fs);
            return this.NextStep;
        }

    }
}
