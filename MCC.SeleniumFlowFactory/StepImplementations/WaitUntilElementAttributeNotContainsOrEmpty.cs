﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class WaitUntilElementAttributeNotContainsOrEmpty : Step
    {
        public WaitUntilElementAttributeNotContainsOrEmpty()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputObject,
                                                            Constants.ParamType.ElementAttribute,
                                                            Constants.ParamType.ElementAttributeContains,
                                                            Constants.ParamType.WaitSeconds });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ElementAttribute,
                                                          Constants.ParamType.ElementAttributeContains });
        }


        public override Step ProcessStep(FlowState fs)
        {
            Console.WriteLine($"{this.GetType().Name}");

            var result = false;
            IWebElement input = GetInputWebElement(); 
            var attrParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ElementAttribute);
            var contParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ElementAttributeContains);
            var waitParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.WaitSeconds);
            int waitSec = 5;
            int.TryParse(waitParam.Value ?? "5", out waitSec);
            int waited = 0;
            if (input != null && !string.IsNullOrEmpty(attrParam.Key) && !string.IsNullOrEmpty(contParam.Key))
            {
                System.Threading.Thread.Sleep(200);
                var attr = contParam.Value;
                while (!string.IsNullOrEmpty(attr) && attr.Contains(contParam.Value) && waited < waitSec)
                {
                    waited++;
                    attr = input.GetAttribute(attrParam.Value);
                    if (string.IsNullOrEmpty(attr) && !attr.Contains(contParam.Value))
                    {
                        result = true;
                        break;
                    }
                    System.Threading.Thread.Sleep(1000);
                }

                Result = result;
                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
