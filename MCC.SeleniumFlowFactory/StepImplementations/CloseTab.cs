﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class CloseTab : Step
    {

        public override Step ProcessStep(FlowState fs)
        {
            Console.WriteLine($"{this.GetType().Name}");

            ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().Window(((FlowStateSelenium)fs).CurrentDriver().WindowHandles.Last()).Close();
            ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().Window(((FlowStateSelenium)fs).CurrentDriver().WindowHandles.First());

            return CreateNextStep(this, fs);
        }

    }
}