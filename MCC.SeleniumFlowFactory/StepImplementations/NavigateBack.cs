﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class NavigateBack : Step
    {
        public override Step ProcessStep(FlowState fs)
        {
            //Console.WriteLine($"{this.GetType().Name}");

            ((FlowStateSelenium)fs).CurrentDriver().Navigate().Back();
                
            return CreateNextStep(this, fs);
        }
    }
}
