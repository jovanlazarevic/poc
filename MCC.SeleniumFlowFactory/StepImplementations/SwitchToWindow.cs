﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class SwitchToWindow : Step
    {
        public SwitchToWindow()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.SwitchTo });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.SwitchTo });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var switchTo = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.SwitchTo);
            if (!string.IsNullOrEmpty(switchTo.Value))
            {
                switch (switchTo.Value)
                {
                    case "Last":
                        var driver = ((FlowStateSelenium)fs).CurrentDriver();
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                        break;
                    default:
                        throw new Exception("Not supported switch");
                }
            }

            return CreateNextStep(this, fs);
        }
    }
}
