﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementById : Step
    {
        public GetElementById()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                          Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            string input = GetInputString();

            if (input != null)
            {
                Result = ((FlowStateSelenium)fs).CurrentDriver().FindElementById(input);
                    
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
