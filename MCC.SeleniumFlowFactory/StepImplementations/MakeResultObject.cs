﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class MakeResultObject : Step
    {
        public MakeResultObject()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputResults,
                                                            Constants.ParamType.AddToResultsReplaceKeys,
                                                            Constants.ParamType.AddToResults });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.InputResults });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var inputResultsParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputResults);
            var addToResultsReplaceKeysParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.AddToResultsReplaceKeys);
            if (!string.IsNullOrEmpty(inputResultsParam.Key))
            {
                // Key replacements
                var resultNames = inputResultsParam.Value.Split(',');
                var resultStoreNames = inputResultsParam.Value.Split(',');
                if (!string.IsNullOrEmpty(addToResultsReplaceKeysParam.Key))
                {
                    resultStoreNames = addToResultsReplaceKeysParam.Value.Split(',');
                }
                // 
                var resultObject = new List<KeyValuePair<string, object>>();
                for (var i = 0; i<resultNames.Count(); i++)
                {
                    var resultName = resultNames[i];
                    var storeName = resultStoreNames[i];

                    var result = GetResult(resultName);

                    resultObject.Add(new KeyValuePair<string, object>(storeName, result));
                }
                if (resultObject.Count > 0)
                {
                   Result = resultObject;
                    // Save result
                    SaveInFlowStateResults();
                }
            }

            return CreateNextStep(this, fs);
        }
    }
}
