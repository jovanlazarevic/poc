﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    /// <summary>
    /// Using secondary web driver instance
    /// </summary>
    public class GetUrlInnerHtmlHash : Step
    {
        public GetUrlInnerHtmlHash()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            //Console.WriteLine($"{this.GetType().Name}");

            var urlParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputObject);
            var url = !string.IsNullOrEmpty(urlParam.Value) ? (string)GetResult(urlParam.Value) : ((FlowStateSelenium)fs).RootUrl;
            if (!string.IsNullOrEmpty(url))
            {
                // Empty page first
                ((FlowStateSelenium)fs).TemporaryDriver.Navigate().GoToUrl("about:blank");
                ((FlowStateSelenium)fs).TemporaryDriver.Navigate().GoToUrl(url);

                System.Threading.Thread.Sleep(1000);

                IWebElement webElement = ((FlowStateSelenium)fs).TemporaryDriver.FindElementsByXPath("//body").FirstOrDefault();

                var pageContent = webElement.GetAttribute("innerHTML");

                Result = CryptoHashHelper.GetHashSha256(pageContent.Trim());

                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }
                
            return CreateNextStep(this, fs);
        }
    }
}
