﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class SetElementAttribute : Step
    {
        public SetElementAttribute()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputObject,
                                                            Constants.ParamType.ElementAttribute,
                                                            Constants.ParamType.InputValue,
                                                            Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ElementAttribute,
                                                          Constants.ParamType.InputValue });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                var attrParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ElementAttribute);
                var attrValueParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputValue);
                if (!string.IsNullOrEmpty(attrParam.Key) && !string.IsNullOrEmpty(attrValueParam.Key))
                {
                    ((FlowStateSelenium)fs).CurrentDriver()
                        .ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);", input, attrParam.Value, attrValueParam.Value);

                    Result = input.GetAttribute(attrParam.Value);
                }
                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
