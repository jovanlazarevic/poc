﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class SwitchToFrame : Step
    {
        public SwitchToFrame()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.SwitchTo,
                                                            Constants.ParamType.ByXPath,
                                                            Constants.ParamType.AddOrUpdateResult});
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.SwitchTo });

        }

        public override Step ProcessStep(FlowState fs)
        {
            var switchTo = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.SwitchTo);
            if (!string.IsNullOrEmpty(switchTo.Value))
            {
                var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
                IWebElement input = GetInputWebElement();
                switch (switchTo.Value)
                {
                    case "FrameWithoutSavingResult":
                        Result = GetXPathResult(null, fs, xPath.Value);
                        if (Result != null)
                        {
                            ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().Frame((IWebElement)Result);
                        }
                        Result = null;
                        break;
                    case "Frame":
                        Result = GetXPathResult(input, fs, xPath.Value);
                        if (Result != null)
                        {
                            ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().Frame((IWebElement)Result);
                        }
                        break;
                    case "ParentFrame":
                        ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().ParentFrame();
                        Result = null;
                        break;
                    default:
                        throw new Exception("Not supported switch");
                }
                SaveInFlowStateResults();
            }

            return CreateNextStep(this, fs);
        }

        private IWebElement GetXPathResult(IWebElement input, FlowState fs, string xpath)
        {
            IWebElement result = null;
            if (!string.IsNullOrEmpty(xpath))
            {
                if (input != null)
                {
                    result = input.FindElements(By.XPath(xpath)).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                }
                else
                {
                    result = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                }
            }
            return result;
        }
    }
}
