﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GoToUrl : Step
    {
        public GoToUrl()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.Url, Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            //Console.WriteLine($"{this.GetType().Name}");

            var urlParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.Url);
            var inputParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputObject);
            var url = string.Empty;
            if (!string.IsNullOrEmpty(urlParam.Value))
            {
                url = urlParam.Value;
            }
            else if (!string.IsNullOrEmpty(inputParam.Value))
            {
                url = GetInputString();
            }
            else
            {
                url = ((FlowStateSelenium)fs).RootUrl;
            }
            if (!string.IsNullOrEmpty(url))
            {
                ((FlowStateSelenium)fs).CurrentDriver().Navigate().GoToUrl(url);
                Result = url;
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception("GetElementText: Incorrect input params");
            }
                
            return CreateNextStep(this, fs);
        }
    }
}
