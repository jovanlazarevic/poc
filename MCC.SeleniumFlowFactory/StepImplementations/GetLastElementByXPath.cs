﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class GetLastElementByXPath : Step
    {
        public GetLastElementByXPath()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                            Constants.ParamType.ByXPath,
                                                            Constants.ParamType.ByAlternateXPath,
                                                            Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByXPath });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();

            var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
            ((FlowStateSelenium)fs).CurrentDriver().Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            List<IWebElement> resultList = null;
            resultList = GetResultList(input, fs, xPath.Value);
            if (resultList.Count == 0)
            {
                xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByAlternateXPath);
                if (!string.IsNullOrEmpty(xPath.Value))
                    resultList = GetResultList(input, fs, xPath.Value);
            }

            if (resultList.Count > 0)
                Result = resultList.Last();
            else
                Result = null;

            SaveInFlowStateResults();

            return CreateNextStep(this, fs);
        }

        private List<IWebElement> GetResultList(IWebElement input, FlowState fs, string xpath)
        {
            List<IWebElement> resultList = null;
            if (!string.IsNullOrEmpty(xpath))
            {
                if (input != null && input is IWebElement)
                {
                    resultList = input.FindElements(By.XPath(xpath)).ToList();
                }
                else
                {
                    resultList = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).ToList();
                }
            }
            return resultList;
        }

    }
}
