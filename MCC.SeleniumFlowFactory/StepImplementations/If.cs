﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class If : Step
    {
        public If()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputObject,
                                                            Constants.ParamType.EqualsTo,
                                                            Constants.ParamType.ContainsIn,
                                                            Constants.ParamType.EndsWith,
                                                            Constants.ParamType.NotEndsWith,
                                                            Constants.ParamType.NotEqualsTo,
                                                            Constants.ParamType.EqualsToObject,
                                                            Constants.ParamType.NotEqualsToObject,
                                                            Constants.ParamType.NotContainsInResultsKeys,
                                                            Constants.ParamType.AddOrUpdateResult,
                                                            Constants.ParamType.IsVisible,
                                                            Constants.ParamType.IsNotVisible});
        }

        public override Step ProcessStep(FlowState fs)
        {
            //Console.WriteLine($"{this.GetType().Name}");

            bool result = false;
            string inputStr = GetInputString();
            string inputObj = GetInputObject()?.ToString();
            var compareParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsTo ||
                                                                         x.Key == Constants.ParamType.NotEqualsTo ||
                                                                         x.Key == Constants.ParamType.ContainsIn ||
                                                                         x.Key == Constants.ParamType.EndsWith ||
                                                                         x.Key == Constants.ParamType.NotEndsWith ||
                                                                         x.Key == Constants.ParamType.EqualsToObject ||
                                                                         x.Key == Constants.ParamType.NotContainsInResultsKeys ||
                                                                         x.Key == Constants.ParamType.IsVisible ||
                                                                         x.Key == Constants.ParamType.IsNotVisible ||
                                                                         x.Key == Constants.ParamType.NotEqualsToObject);
            if (!string.IsNullOrEmpty(compareParam.Key))
            {
                switch (compareParam.Key)
                {
                    case Constants.ParamType.EqualsTo:
                        var equalsToParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsTo);
                        if (inputStr == compareParam.Value)
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.NotEqualsTo:
                        var notEqualsToParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotEqualsTo);
                        if (inputStr != notEqualsToParam.Value)
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.ContainsIn:
                        var containsInParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ContainsIn);
                        var inList = containsInParam.Value.Split(',').ToList();
                        if (inList.Contains(inputStr))
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.EndsWith:
                        var endsWithParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EndsWith);
                        if (!string.IsNullOrEmpty(endsWithParam.Key))
                        { 
                            if (inputStr.EndsWith(endsWithParam.Value))
                            {
                                result = true;
                            }
                        }
                        break;
                    case Constants.ParamType.NotEndsWith:
                        var notEndsWithParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotEndsWith);
                        if (!string.IsNullOrEmpty(notEndsWithParam.Key))
                        {
                            if (!inputStr.EndsWith(notEndsWithParam.Value))
                            {
                                result = true;
                            }
                        }
                        break;
                    case Constants.ParamType.EqualsToObject:
                        var equalsToObjectParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsToObject);
                        if (!string.IsNullOrEmpty(equalsToObjectParam.Key))
                        {
                            var compareTo = GetResult(equalsToObjectParam.Value)?.ToString();
                            if (inputObj == compareTo)
                            {
                                result = true;
                            }
                        }
                        break;
                    case Constants.ParamType.NotEqualsToObject:
                        var notEqualsToObjectParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotEqualsToObject);
                        if (!string.IsNullOrEmpty(notEqualsToObjectParam.Key))
                        {
                            var compareTo = GetResult(notEqualsToObjectParam.Value)?.ToString();
                            if (inputObj != compareTo)
                            {
                                result = true;
                            }
                        }
                        break;
                    case Constants.ParamType.NotContainsInResultsKeys:
                        result = true;
                        var notContainsInResultsKey = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotContainsInResultsKeys);
                        foreach (var r in ((FlowStateSelenium)fs).Results.Where(x => x.Value is List<KeyValuePair<string, object>>))
                        {
                            foreach (var v in r.Value as List<KeyValuePair<string, object>>)
                            {
                                if (v.Key == notContainsInResultsKey.Value)
                                {
                                    if ((v.Value as string) == inputObj)
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case Constants.ParamType.IsVisible:
                        result = true;
                        try
                        {
                            var webElement = (IWebElement)GetInputObject();
                            webElement.Click();
                            webElement.Click();
                        }
                        catch (Exception)
                        {
                            result = false;
                        }
                        break;
                    case Constants.ParamType.IsNotVisible:
                        result = false;
                        try
                        {
                            var webElement = (IWebElement)GetInputObject();
                            webElement.Click();
                            webElement.Click();
                        }
                        catch (Exception)
                        {
                            result = true;
                        }
                        break;
                    default:
                        throw new Exception("Not supported compare");
                }
            }
            else
            {
                throw new Exception("Input text and compare case params have to be specified");
            }

            Result = result;
            SaveInFlowStateResults();
            if (result)
            {
                this.NextStep = CreateInstance(GetNextStepDefinition(this.StepDefinition), fs); // First Step after current step
            }
            else
            {
                int loopCount = 1;
                foreach (var def in fs.StepDefinitions.Where(x => x.StepNumber > this.StepDefinition.StepNumber).OrderBy(x => x.StepNumber))
                {
                    if (def.TypeName == Constants.StepType.If)
                        loopCount++;
                    else if (def.TypeName == Constants.StepType.EndIf)
                        loopCount--;

                    if (loopCount == 0)
                    {
                        this.NextStep = CreateInstance(GetNextStepDefinition(def), fs); // First step after EndIf
                        break;
                    }
                }
            }

            return this.NextStep;
        }
    }
}
