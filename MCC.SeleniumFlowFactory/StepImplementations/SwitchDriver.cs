﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class SwitchDriver : Step
    {
        public SwitchDriver()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.DriverId });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var driverParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.DriverId);
            if (!string.IsNullOrEmpty(driverParam.Value))
            {
                ((FlowStateSelenium)fs).SelectedDriver = driverParam.Value;
            }
            else
            {
                ((FlowStateSelenium)fs).SelectedDriver = "0"; // if not specified, go to default driver instance
            }
            return CreateNextStep(this, fs);
        }
    }
}
