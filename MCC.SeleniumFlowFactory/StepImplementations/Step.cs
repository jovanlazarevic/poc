﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace MCC.SeleniumFlowFactory
{
    public abstract class Step
    {
        protected Step NextStep { get; set; }
        public String StepName { get; set; }
        public StepDefinition StepDefinition { get; set; }
        public FlowState FlowState { get; set; }
        public object Result { get; set; }
        
        // Create next step
        public Step CreateInstance(StepDefinition def, FlowState state)
        {
            if (def != null)
            {
                var existing = FlowState.Steps.FirstOrDefault(x => (x.StepDefinition?.StepNumber ?? 0) == (def?.StepNumber ?? -1));
                if (existing == null)
                {
                    var assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    var instance = Assembly.GetExecutingAssembly().CreateInstance($"{assemblyName}.{def.TypeName}");
                    this.NextStep = (Step)instance;
                    this.NextStep.StepName = def.TypeName;
                    this.NextStep.StepDefinition = def;
                    this.NextStep.FlowState = state;
                    this.FlowState = state;
                }
                else
                {
                    this.NextStep = existing; // Steps inside loops
                }
                state.FinishedPercents.IncrementPercents(Constants.FinishedPercents.MinStepSize);
            }
            else
            {
                this.NextStep = null;
            }
            return this.NextStep;
        }

        public Step CreateNextStep(Step step, FlowState fs)
        {
            step.NextStep = CreateInstance(GetNextStepDefinition(this.StepDefinition), fs);
            return step.NextStep;
        }

        public Step GetPreviousStep()
        {
            return FlowState.Steps.Where(x => (x.StepDefinition?.StepNumber ?? 0) < (this.StepDefinition?.StepNumber ?? 0))
                                  .OrderByDescending(x => (x.StepDefinition?.StepNumber ?? 0))
                                  .FirstOrDefault();
        }

        public object GetResult(string name)
        {
            return ((FlowStateSelenium)FlowState).Results.FirstOrDefault(x => x.Key == name).Value;
        }

        /// <summary>
        /// Returns requested named result or if it is not specified result from previous step
        /// </summary>
        /// <returns></returns>
        public object GetInputObject()
        {
            var prevResultParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputObject);
            var prevResult = (string.IsNullOrEmpty(prevResultParam.Value)
                                        ? GetPreviousStep().Result
                                        : GetResult(prevResultParam.Value));
            return prevResult;
        }
        public IWebElement GetInputWebElement()
        {
            return GetInputObject() is IWebElement ? (IWebElement)GetInputObject() : null;
        }
        public string GetInputString()
        {
            return GetInputObject() is string ? (string)GetInputObject() : null;
        }


        /// <summary>
        /// Get next definition from definitions list
        /// </summary>
        /// <param name="def"></param>
        /// <returns></returns>
        public StepDefinition GetNextStepDefinition(StepDefinition def)
        {
            StepDefinition nextDef = null;
            nextDef = def != null
                        ? FlowState.StepDefinitions.Where(x => x.StepNumber > def.StepNumber).OrderBy(x => x.StepNumber).FirstOrDefault()
                        : FlowState.StepDefinitions.OrderBy(x => x.StepNumber).First();
            
            return nextDef?.StepNumber > 0 ? nextDef : null;
        }

        /// <summary>
        /// Save step Result in global result list if specified by params "AddOrUpdateResult" or "AddToResults"
        /// </summary>
        public void SaveInFlowStateResults()
        {
            var saveResultParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.AddOrUpdateResult);
            if (!string.IsNullOrEmpty(saveResultParam.Value))
            {
                var res = FlowState.Results.FirstOrDefault(x => x.Key == saveResultParam.Value);
                if (!string.IsNullOrEmpty(res.Key))
                {
                    FlowState.Results.Remove(res);
                }
                //if (Result is string)
                //{
                //    Console.WriteLine($"{saveResultParam.Value}: {Result ?? "null"}");
                //}
                FlowState.Results.Add(new KeyValuePair<string, object>(saveResultParam.Value, Result));
            }
            saveResultParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.AddToResults);
            if (!string.IsNullOrEmpty(saveResultParam.Value))
            {
                FlowState.Results.Add(new KeyValuePair<string, object>(saveResultParam.Value, Result));
            }
        }

        // Step children override with unique processing
        public Step ExecuteProcessStep(FlowState fs)
        {
            Step result = null;
            try
            {
                result = ProcessStep(fs);
            }
            catch (Exception ex)
            {
                fs.AddException(ex);
            }
            return result;
        }
        public virtual Step ProcessStep(FlowState fs) { return null; }

        protected List<string> availableParams = new List<string>() { };
        protected List<string> mandatoryParams = new List<string>() { };
        public virtual List<string> CheckParams()
        {
            var result = new List<string>();
            if (this.StepDefinition != null && this.StepDefinition?.Params != null)
            {
                foreach (var param in this.StepDefinition.Params.Where(x => !this.availableParams.Contains(x.Key)))
                {
                    result.Add($"{this.GetType().Name} contains not allowed param \"{param.Key}\". ");
                }

                var stepParams = this.StepDefinition.Params.Select(x => x.Key).ToList();
                foreach (var mp in mandatoryParams)
                {
                    if (!stepParams.Contains(mp))
                    {
                        result.Add($"Mandatory param {mp} was not specified in {this.StepDefinition.TypeName} {this.StepDefinition.Comment}");
                    }
                }
            }
            return result;
        }
        public List<string> GetAvailableParams()
        {
            return availableParams;
        }

    }
}
