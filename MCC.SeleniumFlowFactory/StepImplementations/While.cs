﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class While : Step
    {
        public int StepCount = -1;

        public While()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                            Constants.ParamType.EqualsTo,
                                                            Constants.ParamType.ContainsIn,
                                                            Constants.ParamType.NotEqualsTo,
                                                            Constants.ParamType.EqualsToObject,
                                                            Constants.ParamType.NotEqualsToObject,
                                                            Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            StepCount++;

            if (StepCount == 0)
                Console.WriteLine($"{this.GetType().Name}");

            bool result = false;
            string inputStr = GetInputString();
            string inputObj = GetInputObject()?.ToString();
            var compareParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsTo ||
                                                                         x.Key == Constants.ParamType.NotEqualsTo ||
                                                                         x.Key == Constants.ParamType.ContainsIn ||
                                                                         x.Key == Constants.ParamType.EqualsToObject ||
                                                                         x.Key == Constants.ParamType.NotEqualsToObject);
            if (!string.IsNullOrEmpty(compareParam.Key))
            {
                switch (compareParam.Key)
                {
                    case Constants.ParamType.EqualsTo:
                        var equalsToParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsTo);
                        if (inputStr == compareParam.Value)
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.NotEqualsTo:
                        var notEqualsToParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotEqualsTo);
                        if (inputStr != notEqualsToParam.Value)
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.ContainsIn:
                        var containsInParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ContainsIn);
                        var inList = containsInParam.Value.Split(',').ToList();
                        if (inList.Contains(inputStr))
                        {
                            result = true;
                        }
                        break;
                    case Constants.ParamType.EqualsToObject:
                        var equalsToObjectParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.EqualsToObject);
                        if (!string.IsNullOrEmpty(equalsToObjectParam.Key))
                        {
                            var compareTo = GetResult(equalsToObjectParam.Value)?.ToString();
                            if (inputObj == compareTo)
                            {
                                result = true;
                            }
                        }
                        break;
                    case Constants.ParamType.NotEqualsToObject:
                        var notEqualsToObjectParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.NotEqualsToObject);
                        if (!string.IsNullOrEmpty(notEqualsToObjectParam.Key))
                        {
                            var compareTo = GetResult(notEqualsToObjectParam.Value)?.ToString();
                            if (inputObj != compareTo)
                            {
                                result = true;
                            }
                        }
                        break;
                    default:
                        throw new Exception("Not supported compare");
                }
            }
            else
            {
                throw new Exception("Input text and compare case params have to be specified");
            }

            Result = result;
            SaveInFlowStateResults();
            if (result)
            {
                this.NextStep = CreateInstance(GetNextStepDefinition(this.StepDefinition), fs); // First Step after current step
            }
            else
            {
                int loopCount = 1;
                foreach (var def in fs.StepDefinitions.Where(x => x.StepNumber > this.StepDefinition.StepNumber).OrderBy(x => x.StepNumber))
                {
                    if (def.TypeName == Constants.StepType.While)
                        loopCount++;
                    else if (def.TypeName == Constants.StepType.WhileEnd)
                        loopCount--;

                    if (loopCount == 0)
                    {
                        this.NextStep = CreateInstance(GetNextStepDefinition(def), fs); // First step after WhileEnd
                        break;
                    }
                }
            }

            return this.NextStep;
        }

     }
}
