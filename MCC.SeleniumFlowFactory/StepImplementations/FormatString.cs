﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FormatString : Step
    {
        public FormatString()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.StringFormat,
                                                          Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.StringFormat });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var result = string.Empty;
            var param = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.StringFormat);
            if (!string.IsNullOrEmpty(param.Key))
            {
                var placeHolders = param.Value.Split('{').ToList();
                placeHolders.ForEach(x => x = x.IndexOf('}') > 0 ? x.Substring(0, x.IndexOf('}')-1) : "");
                result = param.Value;
                foreach (var p in placeHolders.Where(x => !string.IsNullOrEmpty(x) && x.Contains('}')))
                {
                    var ph = p.Substring(0, p.IndexOf("}"));
                    var str = (string)GetResult(ph) ?? string.Empty;
                    result = result.Replace($"{{{ph}}}", str);
                }
                Result = result;

                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
