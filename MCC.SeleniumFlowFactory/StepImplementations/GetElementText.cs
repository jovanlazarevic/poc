﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementText : Step
    {
        public GetElementText()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                //Result = input.Text;
                // HtmlAgilityPack
                /*var pageContent = input.GetAttribute("innerHTML"); 
                var pageDoc = new HtmlDocument();
                pageDoc.LoadHtml(pageContent);
                var result = pageDoc.DocumentNode.InnerText;
                */
                // Internal Helper
                /*var HtmlToText = new HtmlToText();
                var pageContent = input.GetAttribute("innerHTML");
                var result = HtmlToText.Convert(pageContent);
                Result = result.Trim();
                */
                Result = input.Text;
            }
            else
            {
                Result = null;
            }

            // Save result
            SaveInFlowStateResults();


            return CreateNextStep(this, fs);
        }
    }
}
