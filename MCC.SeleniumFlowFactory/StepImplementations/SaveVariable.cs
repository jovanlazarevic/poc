﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class SaveVariable : Step
    {
        public SaveVariable()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.InputValue, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var inputValueParam = this.StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputValue);
            if (inputValueParam.Key != null)
            {
                Result = inputValueParam.Value;
            }
            else
            {
                IWebElement inputWeb = GetInputWebElement();
                string inputString = GetInputString();
                if (inputWeb != null)
                {
                    Result = inputWeb;
                }
                else if (!string.IsNullOrEmpty(inputString))
                {
                    Result = inputString;
                }
            }
            // Save result
            SaveInFlowStateResults();


            return CreateNextStep(this, fs);
        }
    }
}
