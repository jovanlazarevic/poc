﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class StringReplace : Step
    {
        public StringReplace()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputObject,
                                                            Constants.ParamType.FindString, 
                                                            Constants.ParamType.ReplaceString, 
                                                            Constants.ParamType.AddOrUpdateResult });
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.FindString,
                                                          Constants.ParamType.ReplaceString });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var result = string.Empty;
            var input = GetInputString();
            var findParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.FindString);
            var replaceParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ReplaceString);
            if (!string.IsNullOrEmpty(input) && !string.IsNullOrEmpty(findParam.Key))
            {
                Result = input.Replace(findParam.Value, replaceParam.Value);

                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
