﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementHash : Step
    {
        public GetElementHash()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                //var pageContent = input.GetAttribute("innerHTML"); // Contains invisible elements
                var pageContent = input.Text;
                Result = CryptoHashHelper.GetHashSha256(pageContent.Trim());

                // Save result
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception("GetElementHash: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
