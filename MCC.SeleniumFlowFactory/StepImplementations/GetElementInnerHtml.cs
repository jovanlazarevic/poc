﻿using MCC.SeleniumFlowFactory.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace MCC.SeleniumFlowFactory
{
    public class GetElementInnerHtml : Step
    {
        public GetElementInnerHtml()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject, Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                Result = input.GetAttribute("innerHTML");
            }
            else
            {
                Result = null;
            }

            // Save result
            SaveInFlowStateResults();


            return CreateNextStep(this, fs);
        }
    }
}
