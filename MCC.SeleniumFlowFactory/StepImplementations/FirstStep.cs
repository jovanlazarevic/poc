﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FirstStep : Step
    {
        public override Step ProcessStep(FlowState fs)
        {
            FlowState = fs;

            return CreateNextStep(this, fs);
        }
    }
}
