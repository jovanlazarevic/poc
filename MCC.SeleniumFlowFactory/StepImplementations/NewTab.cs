﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;

namespace MCC.SeleniumFlowFactory
{
    public class NewTab : Step
    {
        public NewTab()
        {
            availableParams.AddRange(new List<string>() {   Constants.ParamType.InputObject });
        }

        public override Step ProcessStep(FlowState fs)
        {
            Console.WriteLine($"{this.GetType().Name}");

            IWebElement input = GetInputWebElement();
            if (input != null)
            {
                Actions builder = new Actions(((FlowStateSelenium)fs).CurrentDriver());

                builder.KeyDown(Keys.Control)
                    .Click(input)
                    .KeyUp(Keys.Control).Build().Perform();
            }
            else
            {
                throw new Exception($"{this.GetType().Name}: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }
    }
}
