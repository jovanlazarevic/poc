﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class WaitUntilElementIsVisible : Step
    {
        public WaitUntilElementIsVisible()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                            Constants.ParamType.ByXPath,
                                                            Constants.ParamType.ByAlternateXPath,
                                                            Constants.ParamType.AddOrUpdateResult,
                                                            Constants.ParamType.WaitSeconds});
            mandatoryParams.AddRange(new List<string>() { Constants.ParamType.ByXPath });
        }

        public override Step ProcessStep(FlowState fs)
        {
            IWebElement input = GetInputWebElement();
            var xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
            if (!string.IsNullOrEmpty(xPath.Value?.ToString()))
            {
                ((FlowStateSelenium)fs).CurrentDriver().Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 10);
                var waitParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.WaitSeconds);
                int waitSec = 5;
                int.TryParse(waitParam.Value ?? "5", out waitSec);
                int waited = 0;
                Result = null;
                while (Result == null && waited < waitSec)
                {
                    System.Threading.Thread.Sleep(1000);
                    waited += 1;
                    xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByXPath);
                    Result = GetXPathResult(input, fs, xPath.Value, ref waited);
                    if (Result == null)
                    {
                        xPath = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.ByAlternateXPath);
                        Result = GetXPathResult(input, fs, xPath.Value, ref waited);
                    }
                }
                SaveInFlowStateResults();
            }
            else
            {
                throw new Exception("GetElementByXPath: Incorrect input params");
            }

            return CreateNextStep(this, fs);
        }

        private IWebElement GetXPathResult(IWebElement input, FlowState fs, string xpath, ref int waited)
        {
            IWebElement result = null;
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                if (!string.IsNullOrEmpty(xpath))
                {
                    if (input != null)
                    {
                        result = input.FindElements(By.XPath(xpath)).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                    }
                    else
                    {
                        result = ((FlowStateSelenium)fs).CurrentDriver().FindElementsByXPath(xpath).FirstOrDefault(); // if there are no elements it will return empty list instead of exception
                    }
                }
                sw.Stop();
                waited += (int)sw.Elapsed.TotalSeconds;
            }
            catch (Exception)
            {
                // Ignore timeout exception and try again
                sw.Stop();
                waited += (int)sw.Elapsed.TotalSeconds;
            }
            return result;
        }

    }
}
