﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Linq;

namespace MCC.SeleniumFlowFactory
{
    public class SwitchToNewTab : Step
    {

        public override Step ProcessStep(FlowState fs)
        {
            ((FlowStateSelenium)fs).CurrentDriver().SwitchTo().Window(((FlowStateSelenium)fs).CurrentDriver().WindowHandles.Last());
           
            return CreateNextStep(this, fs);
        }
    }
}
