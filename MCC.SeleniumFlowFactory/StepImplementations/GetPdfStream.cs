﻿using MCC.SeleniumFlowFactory.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OpenQA.Selenium.Chrome;

namespace MCC.SeleniumFlowFactory
{
    public class GetPdfStream : Step
    {
        public GetPdfStream()
        {
            availableParams.AddRange(new List<string>() { Constants.ParamType.InputObject,
                                                          Constants.ParamType.Url,
                                                          Constants.ParamType.AddOrUpdateResult });
        }

        public override Step ProcessStep(FlowState fs)
        {
            var urlParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.Url);
            var inputParam = StepDefinition.Params.FirstOrDefault(x => x.Key == Constants.ParamType.InputObject);
            var url = string.Empty;
            if (!string.IsNullOrEmpty(urlParam.Value))
            {
                url = urlParam.Value;
            }
            else if (!string.IsNullOrEmpty(inputParam.Value))
            {
                url = GetInputString();
            }
            else
            {
                url = ((FlowStateSelenium)fs).RootUrl;
            }
            if (!string.IsNullOrEmpty(url))
            {
                using (WebClientWithTimeout client = new WebClientWithTimeout())
                {
                    MemoryStream stream = null;
                    var bytes = client.DownloadData(url);
                    if (bytes[0] == 0x25 && bytes[1] == 0x50 && bytes[2] == 0x44 && bytes[3] == 0x46) // Valid pdf header
                    {
                        stream = new MemoryStream(bytes);
                    }
                    else // Try to get file using Selenium Chrome 
                    {
                        var chromeDriver = ((FlowStateSelenium)fs).GetStreamDriver();
                        chromeDriver.Navigate().GoToUrl(url);
                        System.Threading.Thread.Sleep(5000);
                        //chromeDriver.Close();
                        //chromeDriver.Dispose();

                        // Find just downloaded file - because file name is different than last part of url
                        var downloadFolder = ((FlowStateSelenium)fs).TempDownloadFilePath;
                        DirectoryInfo directory = new DirectoryInfo(downloadFolder);
                        try
                        {
                            FileInfo file = directory.GetFiles("*.pdf", SearchOption.AllDirectories).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
                            if (file != null)
                            {
                                using (FileStream fileStream = new FileStream(Path.Combine(file.DirectoryName, file.Name), FileMode.Open, FileAccess.Read))
                                {
                                    // Load file to MemoryStream
                                    stream = new MemoryStream();
                                    fileStream.CopyTo(stream);
                                    fileStream.Close();
                                }
                                // Delete the file
                                File.Delete(file.FullName);
                            }
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }

                    Result = stream;
                }

                SaveInFlowStateResults();
            }

            return CreateNextStep(this, fs);
        }


    }
}
