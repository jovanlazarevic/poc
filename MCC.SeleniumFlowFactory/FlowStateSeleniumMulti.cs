﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using System;
using System.Collections.Generic;
using System.Text;

namespace MCC.SeleniumFlowFactory
{
    public class FlowStateSeleniumMulti : FlowStateSelenium
    {
        public List<string> RootUrls { get; set; }

        public FlowStateSeleniumMulti(string definitionName, string definitionUrl, string definitionType,
                                      List<StepDefinition> stepDefinitions, List<string> rootUrls, int maxLoopCount = 100, bool isDebug = false)
            :base(definitionName, definitionUrl, definitionType, stepDefinitions, null, maxLoopCount, isDebug)
        {
            RootUrls = rootUrls;
        }

    }
}
