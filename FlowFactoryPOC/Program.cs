﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MCC.SeleniumFlowFactory;

namespace FlowFactoryPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The Flow Factory Proof Of Concept!");
            FlowStateSelenium flowState = null;
            List<StepDefinition> stepDefinitions = null;
            var defName = string.Empty;
            var defUrl = string.Empty;
            var defType = string.Empty;
            List<string> subUrls = null;
        
            try
            {
                #region Locations
                // CodeBook
                /*defName = "CodeBook";
                defUrl = "https://www.codebook.com/listing";
                defType = "Locations";
                stepDefinitions = CodeBookLocations();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.codebook.com/listing", 5000, true);
                */
                // Sterling codifiers
                /*defName = "Sterling Codifiers";
                defUrl = "http://www.sterlingcodifiers.com/#codes";
                defType = "Locations";
                stepDefinitions = SterlingCodifiersLocations();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.sterlingcodifiers.com/#codes", 5000, false);
                */
                // Ecode 360
                /*defName = "Ecode 360";
                defUrl = "http://www.generalcode.com/resources/ecode360-library";
                defType = "Locations";
                stepDefinitions = Ecode360Locations();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.generalcode.com/resources/ecode360-library/", 10, true);
                */
                //FlowParentSelenium fpTest = new FlowParentSelenium();
                //if (fpTest.PrepareAndCheck(flowState))
                //{
                //    GenerateMigrationHelper(flowState);
                //}
                

                // Am Legal
                /*defName = "Am Legal";
                defUrl = "http://www.amlegal.com/code-library/";
                defType = "Locations";
                stepDefinitions = AmLegalLocations();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/code-library/", 3000, false);
                */
                // Muni Code
                /*defName = "Muni Code";
                defUrl = "https://library.municode.com/#";
                defType = "Locations";
                stepDefinitions = MunicodeLocations();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/#", 4000, true);
                */
                #endregion

                #region SUB-URLS
                
                /*defName = "CodeBook"; // (CodePublishing)
                defUrl = "https://www.codebook.com/listing";
                defType = "SubUrls";
                stepDefinitions = CodeBookSubUrls();
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.codepublishing.com/CA/Arcata/", 15, false);
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.codepublishing.com/PA/CornwallBorough", 5, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions,"http://www.codepublishing.com/AZ/Kingman/", 5, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions,"http://www.codepublishing.com/AZ/Phoenix/", 5, false);
                */

                /*defName = "Sterling Codifiers";
                defUrl = "http://www.sterlingcodifiers.com/#codes";
                defType = "SubUrls";
                stepDefinitions = SterlingCodifiersSubUrls();
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.sterlingcodifiers.com/codebook/index.php?book_id=335", 15, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.sterlingcodifiers.com/codebook/index.php?book_id=836", 15, false);
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.sterlingcodifiers.com/codebook/index.php?book_id=901", 100, false);
                */
                /*defName = "Ecode 360";
                defUrl = "http://www.generalcode.com/resources/ecode360-library";
                defType = "SubUrls";
                stepDefinitions = Ecode360SubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://ecode360.com/BR1787", 3, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://ecode360.com/SO1390", 3, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://ecode360.com/BE0692", 100, false);
                //FlowParentSelenium fpTest = new FlowParentSelenium();
                //if (fpTest.PrepareAndCheck(flowState))
                //{
                //    GenerateMigrationHelper(flowState);
                //}
                */

                defName = "Am Legal";
                defUrl = "http://www.amlegal.com/code-library/";
                defType = "SubUrls";
                //stepDefinitions = AmLegalSubUrls();
                stepDefinitions = AmLegalSubUrls();
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/dallas_tx/", 2, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/apache-junction-ldc_az/", 5, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/sand-point_ak/", 3, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/battle-creek_mi/", 3, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/tontitown_ar/", 3, false);

                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/los-angeles_ca/", 4, true, "C:\\temp", true);  // nisu otvorena stabla  Ne skpulja ADMINISTRATIVE CODE \ Divisions (zbog pogresne ikonice na Level 2)
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/dallas_tx/", 3, true, "C:\\temp", true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/chicago_il/", 4, true, "C:\\temp", true); // OK
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.amlegal.com/codes/client/albuquerque_nm/", 4, true, "C:\\temp", true );  // Otvorena sva stabla OK, puca u hromu
                


                /*defName = "PortlandOregon";
                defUrl = "https://www.portlandoregon.gov/citycode/28148";
                defType = "SubUrls";
                stepDefinitions = OregonPortlandSubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.portlandoregon.gov/citycode/28148", 3, true);
                */

                /*defName = "Muni Code";
                defUrl = "https://library.municode.com/#";
                defType = "SubUrls";
                stepDefinitions = MunicodeSubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/ga/atlanta", 3, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/tx/houston/codes/code_of_ordinances", 3, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/tx/austin/codes/code_of_ordinances", 10, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/tx/austin", 15, false);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/nc/raleigh", 15, true);
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://library.municode.com/co/bow_mar", 100, false);
                */

                // Separate cities (from Daniel)
                /*defName = "King County";
                defUrl = "http://www.kingcounty.gov/council/legislation/kc_code.aspx";
                defType = "SubUrls";
                stepDefinitions = KingCountySubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.kingcounty.gov/council/legislation/kc_code.aspx", 200, true);
                */

                /*defName = "Maricopa County";
                defUrl = "https://www.maricopa.gov/733/Ordinances-Codes";
                defType = "SubUrls";
                stepDefinitions = MaricopaCountySubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.maricopa.gov/733/Ordinances-Codes", 200, true);
                */

                /*defName = "San Diego";
                defUrl = "https://www.sandiego.gov/city-clerk/officialdocs/legisdocs/muni";
                defType = "SubUrls";
                stepDefinitions = SanDiegoSubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.sandiego.gov/city-clerk/officialdocs/legisdocs/muni", 4, false);
                */

                /*defName = "Eugene";
                defUrl = "https://www.eugene-or.gov/DocumentCenter/Home/Index/262";
                defType = "SubUrls";
                stepDefinitions = EugeneSubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.eugene-or.gov/DocumentCenter/Home/Index/262", 100, false);
                */

                /*defName = "BOISE CITY";
                defUrl = "https://cityclerk.cityofboise.org/city-code/";
                defType = "SubUrls";
                stepDefinitions = BoiseCitySubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://cityclerk.cityofboise.org/city-code/", 300, false);
                */

                /*defName = "Riverside";
                defUrl = "https://www.riversideca.gov/municode/city-charter.asp";
                defType = "SubUrls";
                stepDefinitions = RiversideSubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.riversideca.gov/municode/city-charter.asp", 300, false);
                */
                /*
                defName = "Universal 2";
                defUrl = "Any url with two levels of <a> tags";
                defType = "SubUrls";
                stepDefinitions = Universal2SubUrls();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://www.washingtonpost.com/", 200, true);
                */
                #endregion

                #region Tests
                // Test percent calculation
                //var stepDefinitions = TestPercentCalculation();
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "http://www.codepublishing.com/CA/Arcata/", 10, true);

                // Test - Scraping text in debug mode
                // Ecode360
                //stepDefinitions = Ecode360Text();
                //flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://ecode360.com/8462388", 100, true);
                #endregion


                #region Run FF engine for Location / Sub-Urls

                if (flowState != null)
                {
                    Console.WriteLine($"{DateTime.UtcNow}");
                    Stopwatch sw = Stopwatch.StartNew();
                    try
                    {
                        // Process Work
                        FlowParentSelenium fp = new FlowParentSelenium();
                        if (fp.PrepareAndCheck(flowState))
                        {
                            fp.ProcessWork();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                    Console.WriteLine("");
                    Console.WriteLine($"Total seconds: {sw.ElapsedMilliseconds / 1000.0M}");

                    #region Save Results To Files
                    try
                    {
                        Console.WriteLine("RESULTS");
                        using (System.IO.StreamWriter file =
                                   new System.IO.StreamWriter(@"D:\Development\FlowFactoryPOC\Results\Result.csv", false))
                        {
                            file.WriteLine($"Name: {flowState.DefinitionName}   Url:{flowState.DefinitionUrl}   Type:{flowState.DefinitionType}, ");
                            // Results
                            foreach (var r in flowState.Results.Where(x => x.Value is List<KeyValuePair<string, object>>))
                            {
                                Console.WriteLine($"{r.Key}:");
                                foreach (var v in r.Value as List<KeyValuePair<string, object>>)
                                {
                                    Console.WriteLine($"{v.Key ?? "null"}: {v.Value ?? "null"}");
                                    if (v.Value is string)
                                    {
                                        var vValue = v.Value != null ? ((string)v.Value).Replace(",", " ") : "null";
                                        file.Write($"{v.Key},{vValue ?? "null"},");
                                    }
                                    else
                                    {
                                        file.Write($"{v.Key},{v.Value?.ToString() ?? "null"},");
                                    }
                                }
                                file.WriteLine("");
                            }
                        }

                        if (flowState.IsDebug)
                        {
                            Console.WriteLine("DEBUG");
                            using (System.IO.StreamWriter file =
                                       new System.IO.StreamWriter(@"D:\Development\FlowFactoryPOC\Results\Debug.txt", false))
                            {
                                foreach (var s in flowState.ExecutionDebugSteps)
                                {
                                    var percents = s.Variables.FirstOrDefault()?.FinishedPercents.ToString("0.00");
                                    if (!string.IsNullOrEmpty(percents))
                                        percents += "%";
                                    var stepComment = !string.IsNullOrEmpty(s.StepComment) ? $"// {s.StepComment}" : string.Empty;

                                    file.WriteLine($"{s.DateTime.ToLocalTime()}: {s.StepName ?? "step"}  ({percents})   {stepComment} isSuccessful:{s.IsSuccessful}");
                                    foreach (var v in s.Variables)
                                    {
                                        file.WriteLine($"  {v.ParamName}{(string.IsNullOrEmpty(v.ParamValue) ? "" : ",")}{v.ParamValue}:  {v.ObjectValue}");
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    #endregion
                }

                #endregion



                #region Scraping text  - Reusing phantom driver for multiple sub-urls
                /*
                // Codebook (CodePublishing)
                defName = "CodeBook";
                defUrl = "https://www.codebook.com/listing";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   //"http://www.codepublishing.com/CA/Arcata/html/Arcata01/Arcata01.html",
                   //"http://www.codepublishing.com/CA/Arcata/html/Arcata01/Arcata0101.html#1",
                   //"http://www.codepublishing.com/CA/Arcata/html/Arcata01/Arcata0102.html#2"

                    "http://www.codepublishing.com/PA/CornwallBorough/#!/CornwallBorough02/CornwallBorough02.html",
                    "http://www.codepublishing.com/PA/CornwallBorough/#!/CornwallBorough02/CornwallBorough0201.html",
                    "http://www.codepublishing.com/PA/CornwallBorough/#!/CornwallBorough02/CornwallBorough0202.html"
                };
                stepDefinitions = CodeBookText();
                FlowParentSelenium fpTest = new FlowParentSelenium();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                //if (fpTest.PrepareAndCheck(flowState))
                //{
                //    GenerateMigrationHelper(flowState);
                //}
                

                /*defName = "Sterling Codifiers";
                defUrl = "http://www.sterlingcodifiers.com/#codes";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?id=&pref_id=836&keywords=",
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?id=&adopt_id=836&keywords=",
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?id=&title_id=5732&keywords=",
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?chapter_id=59320",
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?chapter_id=59321",
                   "http://www.sterlingcodifiers.com/codebook/getBookData.php?chapter_id=59322"
                };
                stepDefinitions = SterlingCodifiersText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                /*
                defName = "Ecode 360";
                defUrl = "http://www.generalcode.com/resources/ecode360-library";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                    "https://ecode360.com/8462387",
                   "https://ecode360.com/8462388",
                   "https://ecode360.com/8462426",
                   "https://ecode360.com/8462475",
                   "https://ecode360.com/8462493",
                   "https://ecode360.com/8462494",
                   "https://ecode360.com/8462524",
                   "https://ecode360.com/8462573",
                   "https://ecode360.com/8462595",
                   "https://ecode360.com/8462603",
                   "https://ecode360.com/8462610",
                   "https://ecode360.com/8462618",
                   "https://ecode360.com/8462643",
                   "https://ecode360.com/8462644"
                };
                stepDefinitions = Ecode360Text();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "https://ecode360.com/8462387", 300, false);
                //FlowParentSelenium fpTest = new FlowParentSelenium();
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */
                /*
                defName = "Am Legal";
                defUrl = "http://www.amlegal.com/code-library";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                    //"http://www.amlegal.com/codes/client/sand-point_ak/",
                    //"http://library.amlegal.com/nxt/gateway.dll/Alaska/sandpoint_ak/adoptingordinance?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    //"http://library.amlegal.com/nxt/gateway.dll/Alaska/sandpoint_ak/title1generalprovisions?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    //"http://library.amlegal.com/nxt/gateway.dll/Alaska/sandpoint_ak/title1generalprovisions/chapter1generalprovisions?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    //"http://library.amlegal.com/nxt/gateway.dll/Alaska/sandpoint_ak/title1generalprovisions/chapter2citydata?f=templates$fn=document-frame.htm$3.0$q=$x="

                    "http://www.amlegal.com/codes/client/albuquerque_nm/",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/cityofalbuquerquenewmexicocodeofordinanc?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/ordinance27-1994?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/charterofthecityofalbuquerque?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter1generalprovisions?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter2government?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter3cityemployees?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter4revenueandtaxation?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter5citypropertypurchasesandsales?f=templates$fn=document-frame.htm$3.0$q=$x=",
                    "http://library.amlegal.com/nxt/gateway.dll/New Mexico/albuqwin/chapter6watersewersandstreets?f=templates$fn=document-frame.htm$3.0$q=$x="
                };
                stepDefinitions = AmLegalText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false, "C:\\temp", true);
                //if (fpTest.PrepareAndCheck(flowState))
                //{
                //    GenerateMigrationHelper(flowState);
                //}
                */

                /*defName = "PortlandOregon";
                defUrl = "https://www.portlandoregon.gov/citycode/28148";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://www.portlandoregon.gov/citycode/28205",
                   "https://www.portlandoregon.gov/citycode/article/13411",
                   "https://www.portlandoregon.gov/citycode/article/13412"
                };
                stepDefinitions = OregonPortlandText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                /*defName = "Muni Code";
                defUrl = "https://library.municode.com/#";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=THCOAUTE01",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=SUHITA01",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=CH",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT1GEPR",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-1CIBO",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-2CAFI",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-3CIAU",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-4CIDE",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-5COACAPTECO",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-6PUSAEMMADE",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-7ETFIDI",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-8FIMAOVPO",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-10MUCO",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT2AD_CH2-11REMA",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT3ANRE",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT4BUREPERE",
                   //"https://library.municode.com/tx/austin/codes/code_of_ordinances?nodeId=TIT4BUREPERE_CH4-1ADAR"


                    //"https://library.municode.com/wa/seattle/codes/municipal_code?nodeId=TIT25ENPRHIPR_CH25.32TAHILA",
                    "https://library.municode.com/wa/seattle/codes/municipal_code?nodeId=TAORCO"
                };
                stepDefinitions = MunicodeText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 5, false);
                //if (fpTest.PrepareAndCheck(flowState))
                //{
                //    GenerateMigrationHelper(flowState);
                //}
                */

                /*defName = "King County";
                defUrl = "http://www.kingcounty.gov/council/legislation/kc_code.aspx";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://www.kingcounty.gov/council/legislation/kc_code.aspx",
                    "https://www.kingcounty.gov/council/legislation/kc_code/01_Preface.aspx",
                    "https://www.kingcounty.gov/council/legislation/kc_code/02_Table_of_Contents.aspx",
                    "https://www.kingcounty.gov/council/legislation/kc_code/03_Charter.aspx",
                    "https://www.kingcounty.gov/council/legislation/kc_code/04_Title_1.aspx"
                };
                stepDefinitions = KingCountyText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                /*defName = "Maricopa County";
                defUrl = "https://www.maricopa.gov/733/Ordinances-Codes";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://www.maricopa.gov/DocumentCenter/View/1995/P-03---Smoking-Pollution-Control-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/1996/P-04---Regulation-of-Traffic-for-Special-Events-by-Volunteer-Sheriffs-Posse-Organizations-Ordinan",
                   "https://www.maricopa.gov/DocumentCenter/View/1997/P-05---Residential-Parking-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/1998/P-06---Regulation-of-Barking-Dogs-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/1999/P-07---Trip-Reduction-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/2000/P-08---Reduction-of-Commuter-Use-of-Motor-Vehicles-by-County-Employees-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/2001/P-10---Adult-Oriented-Businesses-and-Adult-Service-Providers-Ordinance-PDF",
                   "https://www.maricopa.gov/DocumentCenter/View/2002/P-11---Abatement-Ordinance-PDF"
                };
                stepDefinitions = MaricopaCountyText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                /*defName = "San Diego";
                defUrl = "https://www.sandiego.gov/city-clerk/officialdocs/legisdocs/muni";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "http://docs.sandiego.gov/municode/TableofContents/TableofContents.pdf",
                    "http://www.sandiego.gov/city-clerk/municipal-code-ch01",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter01/Ch01Art01Division01.pdf",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter01/Ch01Art01Division02.pdf",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter01/Ch01Art01Division03.pdf",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter01/Ch01Art01Division04.pdf",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter01/Ch01Art02Division01.pdf",
                    "http://www.sandiego.gov/city-clerk/municipal-code-ch02",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter02/Ch02Art01Division01.pdf",
                    "http://docs.sandiego.gov/municode/MuniCodeChapter02/Ch02Art02Division01.pdf"

                };
                stepDefinitions = SanDiegoText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */


                /*defName = "Eugene";
                defUrl = "https://www.eugene-or.gov/DocumentCenter/Home/Index/262";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://www.eugene-or.gov/DocumentCenter/View/2688/Chapter-1-General",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2687/Chapter-1-General-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2690/Chapter-2-Administration",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2689/Chapter-2-Administration-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/3755/Chapter-3-Business-Regulation-and-Taxation",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2691/Chapter-3-Business-Regulation-and-Taxation-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2693/Chapter-4-Offenses",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2692/Chapter-4-Offenses-Table-of-Contents",
                   //"https://www.eugene-or.gov/DocumentCenter/View/10390/Chapter-5-Ice-Snow-Route-Map-Oct-2012",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2695/Chapter-5-Traffic",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2694/Chapter-5-Traffic-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2697/Chapter-6-Environment-and-Health",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2696/Chapter-6-Environment-and-Health-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2699/Chapter-7-Public-Improvements",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2698/Chapter-7-Public-Improvements-TOC",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2701/Chapter-8-Structures",
                   //"https://www.eugene-or.gov/DocumentCenter/View/2700/Chapter-8-Structures-TOC",
                   "https://www.eugene-or.gov/DocumentCenter/View/39766/Chapter-9----Land-Use-Legislative-History-2-26-01-thru-6-1-02",
                   "https://www.eugene-or.gov/DocumentCenter/View/2704/Chapter-9-Land-Use",
                   "https://www.eugene-or.gov/DocumentCenter/View/2702/Chapter-9-Land-Use-figures-and-maps-22-mb",
                   "https://www.eugene-or.gov/DocumentCenter/View/2703/Chapter-9-Land-Use-Table-of-Contents" 
                };
                stepDefinitions = EugeneText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */


                /*defName = "Boise City";
                defUrl = "https://cityclerk.cityofboise.org/city-code/";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://cityclerk.cityofboise.org/media/223546/0530.pdf",
                   "https://cityclerk.cityofboise.org/media/223551/0533.pdf",
                   "https://cityclerk.cityofboise.org/media/223556/0535.pdf",
                   "https://cityclerk.cityofboise.org/media/223566/13555_0537.pdf",
                   "https://cityclerk.cityofboise.org/media/223588/0601.pdf",
                   "https://cityclerk.cityofboise.org/media/261939/0602.pdf",
                   "https://cityclerk.cityofboise.org/media/223593/0604.pdf",
                   "https://cityclerk.cityofboise.org/media/223598/0607.pdf",
                   "https://cityclerk.cityofboise.org/media/223603/13561_0610.pdf",
                   "https://cityclerk.cityofboise.org/media/223608/13563_0612.pdf",
                   "https://cityclerk.cityofboise.org/media/223613/13564_0613.pdf",
                   "https://cityclerk.cityofboise.org/media/223618/0614.pdf",
                   "https://cityclerk.cityofboise.org/media/223623/0615.pdf",
                   "https://cityclerk.cityofboise.org/media/223628/13567_0616.pdf"
                };
                stepDefinitions = BoiseCityText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                /*defName = "Riverside";
                defUrl = "https://www.riversideca.gov/municode/city-charter.asp";
                defType = "ScrapingText";
                subUrls = new List<string>()
                {
                   "https://www.riversideca.gov/municode/pdf/Charter/city-charter.pdf",
                   "https://www.riversideca.gov/municode/pdf/01/title1-all-chapters.pdf",
                   "https://www.riversideca.gov/municode/pdf/02/title-2.pdf",
                   "https://www.riversideca.gov/municode/pdf/03/title-3.pdf",
                   "https://www.riversideca.gov/municode/pdf/04/Title%204.pdf",
                   "https://www.riversideca.gov/municode/pdf/05/title-5.pdf",
                   "https://www.riversideca.gov/municode/pdf/06/title-6.pdf"
                };
                stepDefinitions = RiversideText();
                flowState = new FlowStateSelenium(defName, defUrl, defType, stepDefinitions, "", 300, false);
                if (fpTest.PrepareAndCheck(flowState))
                {
                    GenerateMigrationHelper(flowState);
                }
                */

                #endregion

                #region Run FF engine for Scraping text from list of sub-urls
                
                if (subUrls != null)
                {
                    var flowStateMulti = new FlowStateSeleniumMulti(defName, defUrl, defType, stepDefinitions, subUrls, 100, false);
                    try
                    {
                        // Process Work
                        FlowParentSeleniumMulti fp = new FlowParentSeleniumMulti();
                        if (fp.PrepareAndCheck(flowStateMulti))
                        {
                            fp.ProcessWork();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }


                    Console.WriteLine("RESULTS");
                    using (System.IO.StreamWriter file =
                                new System.IO.StreamWriter(@"D:\Development\FlowFactoryPOC\Results\Result.txt", false))
                    {
                        file.WriteLine($"Name: {flowStateMulti.DefinitionName}   Url: {flowStateMulti.DefinitionUrl}   Type: {flowStateMulti.DefinitionType}");
                        file.WriteLine("===========================================================================================");
                        // Results
                        foreach (var r in flowStateMulti.Results.Where(x => x.Value is List<KeyValuePair<string, object>>))
                        {
                            foreach (var v in r.Value as List<KeyValuePair<string, object>>)
                            {
                                if (v.Value is string)
                                {
                                    var vValue = v.Value ?? "null";
                                    file.WriteLine(vValue);
                                    file.WriteLine("===========================================================================================");
                                }
                                else
                                {
                                    file.WriteLine(v.Value?.ToString());
                                    file.WriteLine("===========================================================================================");
                                }
                            }
                            file.WriteLine("");
                        }
                    }
                    if (flowStateMulti.IsDebug)
                    {
                        Console.WriteLine("DEBUG");
                        using (System.IO.StreamWriter file =
                                    new System.IO.StreamWriter(@"D:\Development\FlowFactoryPOC\Results\Debug.txt", false))
                        {
                            foreach (var s in flowStateMulti.ExecutionDebugSteps)
                            {
                                var percents = s.Variables.FirstOrDefault()?.FinishedPercents.ToString("0.00");
                                if (!string.IsNullOrEmpty(percents))
                                    percents += "%";
                                var stepComment = !string.IsNullOrEmpty(s.StepComment) ? $"// {s.StepComment}" : string.Empty;

                                file.WriteLine($"{s.DateTime.ToLocalTime()}: {s.StepName ?? "step"}  ({percents})     {stepComment}");
                                foreach (var v in s.Variables)
                                {
                                    file.WriteLine($"  {v.ParamName}{(string.IsNullOrEmpty(v.ParamValue) ? "" : ",")}{v.ParamValue}:  {v.ObjectValue}");
                                }
                            }
                        }
                    }
                }
                
                #endregion



                #region Available params
                //FlowParentSelenium fp = new FlowParentSelenium();
                //var feParams = fp.GetAvailableParams(Constants.StepType.ForEach);
                //var feEndParams = fp.GetAvailableParams(Constants.StepType.ForEachEnd);
                //var failParams = fp.GetAvailableParams("abcdefgh");

                // test available steps
                //FlowParentSelenium fp = new FlowParentSelenium();
                //var availableSteps = fp.GetAvailableSteps();
                //foreach (var s in availableSteps)
                //{
                //    Console.WriteLine($"StepType: {s.Item1}");
                //    foreach (var p in s.Item2)
                //    {
                //        Console.WriteLine($"  Param: {p}");
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                if (flowState != null)
                {
                    foreach (var r in flowState.Exceptions)
                    {
                        Console.WriteLine($"{r.Key.ToShortTimeString()}: {r.Value}");
                    }
                }
                else
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }

        #region Algorithm Definitions

        #region Collecting Locations

        private static List<StepDefinition> CodeBookLocations()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@id, 'block-yui')]//a[contains(@href, 'http')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "../../h1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationState"));


            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));


            return defs;
        }

        private static List<StepDefinition> MunicodeLocations()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//li[contains(@ng-repeat, 'state in')]/a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationState"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateHref"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "State"));

                defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateHref"));

                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//li[contains(@ng-repeat, 'client in')]/a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CityWebElement"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));
                }
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        private static List<StepDefinition> AmLegalLocations()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach, "States"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'col-sm-4')]//h2/a")); 
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateWebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationState"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateHref"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "City"));

                defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateHref"));

                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Cities"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'col-sm-4')]//h2/a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CityWebElement"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));
                }
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        private static List<StepDefinition> SterlingCodifiersLocations()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            //defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            //defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a[contains(@href, '#codes')]"));
            //defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "WebElement"));

            //defs.Add(new StepDefinition(Constants.StepType.Click));
            //defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "WebElement"));

            // Click is not working in headless mode, so changing attribute is solution
            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='codes']/div[contains(@class, 'row map')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "WebElement"));

            defs.Add(new StepDefinition(Constants.StepType.SetElementAttribute));  
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "WebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, "display: block;"));


            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='search_select_state']/option"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationState"));

            defs.Add(new StepDefinition(Constants.StepType.Click));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "5"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='search_select_city']/option"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CityWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "value"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        private static List<StepDefinition> Ecode360Locations()
        {
            var defs = new List<StepDefinition>();
            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.Url, "http://www.generalcode.com/resources/ecode360-library/"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Main loop"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'codeTitle')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationName"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "..//div[contains(@class, 'codeCounty')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CountyWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CountyWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationCounty"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "../../..//h2"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "StateWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "StateWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.StringReplace));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.FindString, "(return to top)"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationState"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ReplaceString, ""));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "LocationWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "LocationHref"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "LocationName,LocationHref,LocationState,LocationCounty"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "Location"));

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        #endregion

        #region Collecting Sub-urls

        private static List<StepDefinition> CodeBookSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.SwitchToFrame));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "FrameWithoutSavingResult"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//frame[@name='toc']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NavigationFrame"));

            #region Level 1
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "RootUrl"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "//form/p"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Span"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsTo, "+"));

                defs.Add(new StepDefinition(Constants.StepType.Click));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_a_id"));

                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "div{Level1WebElement_a_id}"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementById));

                defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "overflow"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level1Span == "+"

            defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

            /// Level 2 Optimization
            // get last element (same selector as in ForEach)
            defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2 Optimization"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastWebElement"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));

                // get element <a   />
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastWebElement_a"));
                // get attribute href
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHref"));
                // get url content Hash (new StepType combination of getting content by temporarydriver and calculating hash)
                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastHref"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHtmlHash"));
                // if lastElementHash != parentHash 
                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2LastHtmlHash"));

                #region Level 2
                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 2"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Span"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, null));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsTo, "+"));

                defs.Add(new StepDefinition(Constants.StepType.Click));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a_id"));

                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "div{Level2WebElement_a_id}"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementById));

                defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "overflow"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                defs.Add(new StepDefinition(Constants.StepType.EndIf));

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2Span

                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));


                /// Level 3 Optimization
                // get last element (same selector as in ForEach)
                defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 3 Optimization"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastWebElement"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));

                // get element <a   />
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastWebElement_a"));
                // get attribute href
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHref"));
                // get url content Hash 
                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastHref"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHtmlHash"));
                // if lastElementHash != parentHash 
                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3LastHtmlHash"));

                #region Level 3
                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 3"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Href"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Span"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, null));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Span"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsTo, "+"));

                defs.Add(new StepDefinition(Constants.StepType.Click));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_span"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement_a_id"));

                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "div{Level2WebElement_a_id}"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementById));

                defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "overflow"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3Span == "+"

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3Span != null

                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level3Name,Level3Href,Level3HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                /// Level 4
                /// 
                // Optimization
                // get last element (same selector as in ForEach)
                defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 4 Optimization"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastWebElement"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));

                // get element <a   />
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastWebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastWebElement_a"));
                // get attribute href
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastWebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastHref"));
                // get url content Hash 
                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastHref"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastHtmlHash"));
                // if lastElementHash != parentHash 
                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level4LastHtmlHash"));


                #region Level 4
                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 4"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@class, 'ptoc')][1]/p"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4WebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4Href"));
                //
                //    Level 5 :)
                //
                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level4HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level4Name,Level4Href,Level4HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3HtmlHash != Level4HtmlHash

                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                #endregion
                // Leve 4 End

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3HtmlHash != Level4LastHtmlHash

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level4LastWebElement != null

                defs.Add(new StepDefinition(Constants.StepType.EndIf));

                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                #endregion
                // Leve 3 End

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2HtmlHash != Level3LastHtmlHash

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3LastWebElement != null

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level1HtmlHash != Level2LastHtmlHash

                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                #endregion
                // Leve 2 End

                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level1HtmlHash != Level2LastHtmlHash

            defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2LastWebElement != null

            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> OregonPortlandSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            #region View more links

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='main-content']/div/a[@class='view-all-pages']"));
                defs.Add(new StepDefinition(Constants.StepType.Click));
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            #endregion


            #region Level 1

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='main-content']/div/ul/li"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Style"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Style"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, "display: none;"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_a"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                    defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));

                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                    #region Level 2

                    defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level2"));

                    defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                    //defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                    //defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
                    //defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "SaveVariable"));

                    defs.Add(new StepDefinition(Constants.StepType.ForEach));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='main-content']/ul/li/ul/li/p"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Style"));

                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Style"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, "display: none;"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));


                            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                            defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));

                                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                            defs.Add(new StepDefinition(Constants.StepType.EndIf)); 
                        defs.Add(new StepDefinition(Constants.StepType.EndIf)); 
                        
                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 2

                    // If Not found any element with "//*[@id='main-content']/ul/li/ul/li/p"  try with "//*[@id='main-content']/ul/li/p"
                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));

                        defs.Add(new StepDefinition(Constants.StepType.ForEach));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='main-content']/ul/li/p"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                            defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));

                                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                            defs.Add(new StepDefinition(Constants.StepType.EndIf)); 
                        
                        defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 2

                    defs.Add(new StepDefinition(Constants.StepType.EndIf)); 

                    defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));  // If param DriverId not specified, go to default driver ("0")
                    #endregion
                    // Leve 2 End
                defs.Add(new StepDefinition(Constants.StepType.EndIf));
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> MunicodeSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//section[@id='toc']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "//a[@class='btn btn-primary btn-raised']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "BrowseButtonOrContent"));
            {
                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "BrowseButtonOrContent"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "BrowseButtonOrContent"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "BrowseButton_href"));

                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "BrowseButton_href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level1"));

                        defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "BrowseButton_href"));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));

                // Check popup message
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Check popup message"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//button[contains(@class, 'hopscotch-close')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "closePopup"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "closePopup"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.Click));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "closePopup"));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));


                // Browse tree 
                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//section[@id='toc']"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "section_toc"));
                {
                    // Level 1
                    defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "section_toc"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[(contains(@id,'genToc_'))]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Treenode"));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[not(contains(@class, 'anchor-offset'))]"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_a"));

                        defs.Add(new StepDefinition(Constants.StepType.Click));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_a"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_href"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_a_span"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_a_span"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_text"));


                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//mcc-codes-content"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Content"));

                        defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "opacity"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Content"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));

                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1_text,Level1_href,Level1HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));


                        
                        // Level 2 Optimization
                        // get last element (same selector as in ForEach)
                        defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2 optimization"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, ".//*[(contains(@id,'genToc_')) and (contains(@depth,'-1'))]"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastTreenode"));

                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[not(contains(@class, 'anchor-offset'))]"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastTreenode_a"));

                            // Click on last
                            defs.Add(new StepDefinition(Constants.StepType.Click));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode_a"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//mcc-codes-content"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastContent"));

                            defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "opacity"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastContent"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHtmlHash"));

                            // Level 2
                            defs.Add(new StepDefinition(Constants.StepType.If, "Level 2"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2LastHtmlHash"));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2PrevHtmlHash"));

                                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, ".//*[(contains(@id,'genToc_')) and (contains(@depth,'-1'))]"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Treenode"));
                                {
                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[not(contains(@class, 'anchor-offset'))]"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_a"));

                                    defs.Add(new StepDefinition(Constants.StepType.Click));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_a"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_a"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_href"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_a"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_a_span"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_a_span"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_text"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//mcc-codes-content"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Content"));

                                    defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "opacity"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Content"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2PrevHtmlHash"));
                                    {
                                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2_text,Level2_href,Level2HtmlHash"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                        defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2PrevHtmlHash"));

                                        
                                        // Level 3 Optimization
                                        // get last element (same selector as in ForEach)
                                        defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 3 Optimization"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, ".//*[(contains(@id,'genToc_')) and (contains(@depth,'-1'))]"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastTreenode"));

                                        defs.Add(new StepDefinition(Constants.StepType.If));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                        {
                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[not(contains(@class, 'anchor-offset'))]"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastTreenode_a"));

                                            // Click on last
                                            defs.Add(new StepDefinition(Constants.StepType.Click));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode_a"));

                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//mcc-codes-content"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastContent"));

                                            defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "opacity"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                            defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastContent"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHtmlHash"));

                                            // Level 3
                                            defs.Add(new StepDefinition(Constants.StepType.If, "Level 3"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3LastHtmlHash"));
                                            {
                                                defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3PrevHtmlHash"));

                                                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, ".//*[(contains(@id,'genToc_')) and (contains(@depth,'-1'))]"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Treenode"));
                                                {
                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[not(contains(@class, 'anchor-offset'))]"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_a"));

                                                    defs.Add(new StepDefinition(Constants.StepType.Click));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_a"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_a"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_href"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_a"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_a_span"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_a_span"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_text"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//mcc-codes-content"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Content"));

                                                    defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "opacity"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Content"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3HtmlHash"));

                                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3PrevHtmlHash"));
                                                    {
                                                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level3_text,Level3_href,Level3HtmlHash"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                                        defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3PrevHtmlHash"));
                                                    }
                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                }
                                                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                                            }
                                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                            // Level 3 end

                                        }
                                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                        // Level 3 Optimization end
                                        
                                    }
                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                }
                                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                            }
                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                            // Level 2 end
                            
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                        // Level 2 Optimization end
                        
                    }
                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                    // Level 1 end
                }
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                // Browse tree end

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        private static List<StepDefinition> SterlingCodifiersSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.SwitchToFrame));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "Frame"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//frame[@name='leftframe']"));

            #region Level 1
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='da0']/div[contains(@class, 'dTreeNode')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'node')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a/img[contains(@src, 'nolines_plus.gif')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_img"));

                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject, "Save results"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_img"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.Click));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));


                    /// Level 2
                    /// 

                    // Optimization
                    // get last element (same selector as in ForEach)
                    defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2 optimization"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@id, 'da')][1]/div[contains(@class, 'dTreeNode')]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastWebElement"));
                
                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    { 
                        // get element <a   />
                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'node')]"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastWebElement_a"));
                        // get attribute href
                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastWebElement_a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHref"));
                        // get url content Hash (new StepType combination of getting content by temporarydriver and calculating hash)
                        defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastHref"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHtmlHash"));

                        // if lastElementHash != parentHash 
                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2LastHtmlHash"));
                        {
                    
                            #region Level 2
                            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 2"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@id, 'da')][1]/div[contains(@class, 'dTreeNode')]"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'node')]"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a"));

                                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a/img[contains(@src, 'nolines_plus.gif')]"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_img"));

                                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                                defs.Add(new StepDefinition(Constants.StepType.If));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));
                                {
                                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject, "Save results"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_img"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                    {
                                        defs.Add(new StepDefinition(Constants.StepType.Click));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));

                                        /// Level 3 optimization
                                        /// 
                                        /// get last element (same selector as in ForEach)
                                        defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 3 optimization"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@id, 'da')][1]/div[contains(@class, 'dTreeNode')]"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastWebElement"));

                                        defs.Add(new StepDefinition(Constants.StepType.If));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                        {
                                            // get element <a   />
                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'node')]"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastWebElement_a"));
                                            // get attribute href
                                            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastWebElement_a"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHref"));
                                            // get url content Hash (new StepType combination of getting content by temporarydriver and calculating hash)
                                            defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastHref"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHtmlHash"));

                                            // if lastElementHash != parentHash 
                                            defs.Add(new StepDefinition(Constants.StepType.If));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3LastHtmlHash"));
                                            {

                                                #region Level 3
                                                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 3"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "following-sibling::div[contains(@id, 'da')][1]/div[contains(@class, 'dTreeNode')]"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement"));
                                                {
                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'node')]"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement_a"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_a"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Name"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement_a"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Href"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3WebElement"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a/img[contains(@src, 'nolines_plus.gif')]"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3WebElement_img"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Href"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3HtmlHash"));

                                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3HtmlHash"));
                                                    {
                                                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject, "Save results"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level3Name,Level3Href,Level3HtmlHash"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                                        // TODO: Level 4 if needed
                                                    }
                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2HtmlHash != Level3LastHtmlHash
                                                }
                                                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                                                #endregion
                                                // Leve 3 End
                                            }
                                            defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2HtmlHash != Level3LastHtmlHash
                                            // Leve 3 optimization End
                                        }
                                        defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level3LastWebElement != null
                                    }
                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                }
                                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level1HtmlHash != Level2LastHtmlHash
                            }
                            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                            #endregion
                            // Leve 2 End
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level1HtmlHash != Level2LastHtmlHash
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf)); // Level2LastWebElement != null
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> Ecode360SubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'modalClose')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NoButton"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NoButton"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));

                defs.Add(new StepDefinition(Constants.StepType.Click));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NoButton"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id='contentArea']//div[contains(@class, 'tocTitle')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./a[contains(@class, 'titleLink')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement_a"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.StringReplace));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Name"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.FindString, "\r\n"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ReplaceString, " "));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement_a"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver, "Take url Hash only from <body>"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level11"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "WebElement11Content"));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "WebElement11Content"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));
                }
                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level2"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level2"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                    defs.Add(new StepDefinition(Constants.StepType.Wait));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));

                    defs.Add(new StepDefinition(Constants.StepType.ForEach));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id='contentArea']//a[contains(@class, 'lineTitle')]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement_a"));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement_a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver, "Take url Hash only from ,body> but with excluding titleBox (floating div) on the top"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level21"));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id='titleBox']"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "TitleBox"));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "TitleBox"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                            {

                                defs.Add(new StepDefinition(Constants.StepType.SetElementAttribute));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "TitleBox"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, "display:none;"));
                            }
                            defs.Add(new StepDefinition(Constants.StepType.EndIf));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "WebElement21Content"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementHash));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "WebElement21Content"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level2"));

                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.StringReplace));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Name"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.FindString, "\r\n"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ReplaceString, " "));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));

                        defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2PrevHtmlHash"));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

                }
                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> AmLegalSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,RootUrl,Null"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'code-links')]/a[contains(@class, 'code-link btn btn-primary')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ViewCodeButton"));
            {
                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ViewCodeButton_href"));

                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton_href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level1"));

                        defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton_href"));

                        // Prepare temporary driver
                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "TemporaryDriver"));

                        defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "RootUrl"));

                        defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton_href"));

                        defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level1"));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));

                // Switch to window
                defs.Add(new StepDefinition(Constants.StepType.SwitchToWindow));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "Last"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchToFrame));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "FrameWithoutSavingResult"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//frame[@name='contents']"));

                //TODO:  pdfs

                // Check if there are loading nodes
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Check if there are loading nodes"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'childrennode')]//img[contains(@src,'toc-collapsedhascontent.gif')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "HasLoadingNodes"));

                defs.Add(new StepDefinition(Constants.StepType.SaveVariable, "Next command will start without input value"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "EmptyVariable"));

                // Browse tree 
                defs.Add(new StepDefinition(Constants.StepType.ForEach, "Browse tree"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id='/_c']/div/div/div[contains(@class, 'treenode')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "//div[@id='/_c']/div/div/div/div[contains(@class, 'treenode')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CityWebElement"));
                {
                    // Collapsed tree root (e.g. Los Angeles)
                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Check if level 0 plusminus is expanded"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div[contains(@class, 'childrennode')]/div[contains(@class,'treenode')]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Treenode"));

                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "HasLoadingNodes"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Level 0 plusminus"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[@class='plusminus']"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level0PlusminusButton"));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level0PlusminusButton"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.Click, "Click on Level 0 Plusminus img"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level0PlusminusButton"));
                            }
                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));

                    // Level 1
                    defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CityWebElement"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div[contains(@class, 'childrennode')]/div[contains(@class,'treenode')]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Treenode"));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_id"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_span"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_span"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_text"));

                        defs.Add(new StepDefinition(Constants.StepType.FormatString));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level1_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1_href"));

                        defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1_href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1HtmlHash"));

                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1_text,Level1_href,Level1HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Level 1 plusminus"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[@class='plusminus']"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1PlusminusButton"));


                        // Level 2 Optimization
                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Level 2 optimization"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[contains(@src,'toc-leaf.gif')]"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1NodeIcon"));

                        defs.Add(new StepDefinition(Constants.StepType.If, "check if tree node has expandandable icon"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1NodeIcon"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                        {
                            // get last element (same selector as in ForEach)
                            defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2 optimization"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastTreenode"));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "HasLoadingNodes"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.If, "check if Level1 tree node was not expanded"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.IsNotVisible, null));
                                {
                                    defs.Add(new StepDefinition(Constants.StepType.Click));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1PlusminusButton"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div[@class='childrennode']"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ChildrenNode"));

                                    defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ChildrenNode"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "display: none;"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                    defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2 optimization"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastTreenode"));
                                }
                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                            }
                            defs.Add(new StepDefinition(Constants.StepType.EndIf));

                            defs.Add(new StepDefinition(Constants.StepType.If));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2LastTreenode"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_id"));

                                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level2_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_href"));

                                defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_href"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2LastHtmlHash"));

                                // Level 2
                                defs.Add(new StepDefinition(Constants.StepType.If, "Level 2"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2LastHtmlHash"));
                                {
                                    defs.Add(new StepDefinition(Constants.StepType.ForEach));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Treenode"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Treenode"));
                                    {
                                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_id"));

                                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_span"));

                                        defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_span"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_text"));

                                        defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level2_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2_href"));

                                        defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2_href"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2HtmlHash"));

                                        defs.Add(new StepDefinition(Constants.StepType.If));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1HtmlHash"));
                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level2HtmlHash"));
                                        {
                                            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2_text,Level2_href,Level2HtmlHash"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                            // Level 3 Optimization
                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Level 3 optimization"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[contains(@src,'toc-leaf.gif')]"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2NodeIcon"));

                                            defs.Add(new StepDefinition(Constants.StepType.If, "check if tree node has expandandable icon"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2NodeIcon"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                                            {
                                                // get last element (same selector as in ForEach)
                                                defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 3 optimization"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastTreenode"));

                                                defs.Add(new StepDefinition(Constants.StepType.If));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "HasLoadingNodes"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                {
                                                    defs.Add(new StepDefinition(Constants.StepType.If, "check if tree node was not expanded"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                                                    {
                                                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[@class='plusminus']"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2PlusminusButton"));

                                                        defs.Add(new StepDefinition(Constants.StepType.If));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2PlusminusButton"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                        {
                                                            defs.Add(new StepDefinition(Constants.StepType.Click, "Click on Level 2 Plusminus img"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2PlusminusButton"));

                                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div[@class='childrennode']"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ChildrenNode"));

                                                            defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ChildrenNode"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "display: none;"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                                            defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 3 optimization"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastTreenode"));
                                                        }
                                                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                    }
                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                }
                                                defs.Add(new StepDefinition(Constants.StepType.EndIf));

                                                defs.Add(new StepDefinition(Constants.StepType.If));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                {
                                                    defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3LastTreenode"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_id"));

                                                    defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level3_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_href"));

                                                    defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_href"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHtmlHash"));

                                                    // Level 3
                                                    defs.Add(new StepDefinition(Constants.StepType.If, "Level 3"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3LastHtmlHash"));
                                                    {
                                                        defs.Add(new StepDefinition(Constants.StepType.ForEach));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Treenode"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3Treenode"));
                                                        {
                                                            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_id"));

                                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_span"));

                                                            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_span"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_text"));

                                                            defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level3_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3_href"));

                                                            defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3_href"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3HtmlHash"));

                                                            defs.Add(new StepDefinition(Constants.StepType.If));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2HtmlHash"));
                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level3HtmlHash"));
                                                            {
                                                                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level3_text,Level3_href,Level3HtmlHash"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                                                                // Level 4 Optimization
                                                                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Level 4 optimization"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[contains(@src,'toc-leaf.gif')]"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3NodeIcon"));

                                                                defs.Add(new StepDefinition(Constants.StepType.If, "check if tree node has expandandable icon"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3NodeIcon"));
                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                                                                {
                                                                    // get last element (same selector as in ForEach)
                                                                    defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 4 optimization"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastTreenode"));

                                                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "HasLoadingNodes"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                                    {
                                                                        defs.Add(new StepDefinition(Constants.StepType.If, "check if tree node was not expanded"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastTreenode"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                                                                        {
                                                                            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./img[@class='plusminus']"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "PlusminusButton"));

                                                                            defs.Add(new StepDefinition(Constants.StepType.If));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "PlusminusButton"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                                            {
                                                                                defs.Add(new StepDefinition(Constants.StepType.Click));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "PlusminusButton"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div[@class='childrennode']"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ChildrenNode"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementAttributeNotContainsOrEmpty));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ChildrenNode"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "style"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttributeContains, "display: none;"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 4 optimization"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4LastTreenode"));
                                                                            }
                                                                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                        }
                                                                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                    }
                                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));

                                                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastTreenode"));
                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                                                    {
                                                                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4LastTreenode"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_id"));

                                                                        defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level4_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_href"));

                                                                        defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4_href"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level3LastHtmlHash"));

                                                                        // Level 4
                                                                        defs.Add(new StepDefinition(Constants.StepType.If, "Level 4"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                                                                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level4LastHtmlHash"));
                                                                        {
                                                                            defs.Add(new StepDefinition(Constants.StepType.ForEach));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3Treenode"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./div/div[contains(@class,'null')]"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByAlternateXPath, "./div/div[contains(@class,'treenode')]"));
                                                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4Treenode"));
                                                                            {
                                                                                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4Treenode"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "id"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_id"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4Treenode"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "./span"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_span"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4_span"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_text"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "http://library.amlegal.com/nxt/gateway.dll/{Level4_id}?f=templates$fn=document-frame.htm$3.0$q=$x="));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4_href"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.GetUrlInnerHtmlHash));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level4_href"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level4HtmlHash"));

                                                                                defs.Add(new StepDefinition(Constants.StepType.If));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level3HtmlHash"));
                                                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, "Level4HtmlHash"));
                                                                                {
                                                                                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level4_text,Level4_href,Level4HtmlHash"));
                                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                                                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                                                                                }
                                                                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                            }
                                                                            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                                                                        }
                                                                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                        // Level 4 end
                                                                    }
                                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                }
                                                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                                // Level 4 Optimization end
                                                            }
                                                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                        }
                                                        defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                                                    }
                                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                                    // Level 3 end

                                                }
                                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                            }
                                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                            // Level 3 Optimization end

                                        }
                                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                    }
                                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                                }
                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                // Level 2 end
                            }
                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                        // Level 2 Optimization end

                    }
                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                    // Level 1 end
                }
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                // Browse tree end

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

            return defs;
        }

        private static List<StepDefinition> KingCountySubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'list-group') and contains(@class, 'collapsed')]/a[contains(@class, 'list-group-item')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> MaricopaCountySubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "15"));

            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'sortableWidgetTitle')]//a[contains(@class, 'pdf')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); // Level 1
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> SanDiegoSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id='mainContent']/ul/li//a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level2"));

                defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                #region Level 2
                defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NoMorePages"));

                defs.Add(new StepDefinition(Constants.StepType.While, "Level 2 pages"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NoMorePages"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level items"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'entry__content')]/div//tr/td//a"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                        defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

                    defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Check if Next button exists"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a/img[contains(@src, 'nav_next.gif')]"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NextButton"));

                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NextButton"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, "True"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NoMorePages"));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));

                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NextButton"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath, "Check if a above Next button exists"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NextButton"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "../a"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "NextButton_a"));

                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NextButton"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                        {

                            defs.Add(new StepDefinition(Constants.StepType.Click));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "NextButton_a"));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                }
                defs.Add(new StepDefinition(Constants.StepType.WhileEnd));
                #endregion
                // Leve 2 End

                defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd)); 
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> EugeneSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 't-widget')]/table//tr/td//a[@class='pdf']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> BoiseCitySubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'autoindex ')]/ul/li/a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EndsWith, "#"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.Click));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));


            #region Level 1  
            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 2"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'autoindex ')]/ul/li/ul/li/a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEndsWith, "media/294678/binder1.pdf"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
            #endregion
            // Leve 1 End

            return defs;
        }

        private static List<StepDefinition> RiversideSubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id = 'navlist']/div/ul/li/a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEndsWith, ".pdf"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level 2"));

                    defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                    #region Level 2  
                    defs.Add(new StepDefinition(Constants.StepType.GetLastElementByXPath, "Level 2"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'h2bg')]//a"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                            defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    #endregion
                    // Leve 2 End

                    defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));



            return defs;
        }

        private static List<StepDefinition> Universal2SubUrls()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 1"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1WebElement"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Href"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1WebElement"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "Level1: {Level1Name}"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Name"));

                defs.Add(new StepDefinition(Constants.StepType.If));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                {
                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, ""));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotContainsInResultsKeys, "Href"));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level1Hash"));

                            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level1Name,Level1Href,Level1Hash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));

                          
                            defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.DriverId, "Level 2"));

                            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level1Href"));

                            // Leve 2 
                            #region Level 2  
                            defs.Add(new StepDefinition(Constants.StepType.ForEach, "Level 2"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//a"));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2WebElement"));
                            {
                                defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Href"));

                                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2WebElement"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                                defs.Add(new StepDefinition(Constants.StepType.FormatString));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.StringFormat, "Level2: {Level2Name}"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Name"));

                                defs.Add(new StepDefinition(Constants.StepType.If));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                                {
                                    defs.Add(new StepDefinition(Constants.StepType.If));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsTo, ""));
                                    {
                                        defs.Add(new StepDefinition(Constants.StepType.If));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotContainsInResultsKeys, "Href"));
                                            {
                                                defs.Add(new StepDefinition(Constants.StepType.GetUrlContentHash));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Level2Href"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Level2Hash"));

                                                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "Level2Name,Level2Href,Level2Hash"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResultsReplaceKeys, "Name,Href,HtmlHash"));
                                                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "SubUrl"));
                                            }
                                            defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                    }
                                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                                }
                                defs.Add(new StepDefinition(Constants.StepType.EndIf));
                            }
                            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

                            #endregion
                            // Leve 2 End

                            defs.Add(new StepDefinition(Constants.StepType.SwitchDriver));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));
                }
                defs.Add(new StepDefinition(Constants.StepType.EndIf));
            }
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

  
  
            return defs;
        }

        #endregion

        #region Scraping text

        private static List<StepDefinition> Ecode360Text()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id ='text']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> AmLegalText()
        {
            var defs = new List<StepDefinition>();
            
            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, null));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CookiesPrepared"));

            // For first url in the list, click on rootUrl, then "View Code" button 
            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "CookiesPrepared"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));
            {
                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'code-links')]/a[contains(@class, 'code-link btn btn-primary')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ViewCodeButton"));
                {
                    defs.Add(new StepDefinition(Constants.StepType.If));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                    {
                        defs.Add(new StepDefinition(Constants.StepType.GetElementAttribute));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ElementAttribute, "href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ViewCodeButton_href"));

                        defs.Add(new StepDefinition(Constants.StepType.If));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton_href"));
                        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEqualsToObject, null));
                        {
                            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
                            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ViewCodeButton_href"));
                        }
                        defs.Add(new StepDefinition(Constants.StepType.EndIf));
                    }
                    defs.Add(new StepDefinition(Constants.StepType.EndIf));

                    // Switch to window
                    defs.Add(new StepDefinition(Constants.StepType.SwitchToWindow));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "Last"));

                    defs.Add(new StepDefinition(Constants.StepType.SaveVariable));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputValue, "Yes"));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "CookiesPrepared"));
                }
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
            }
            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> MunicodeText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.WaitUntilElementIsVisible));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id ='codesContent']//ul[contains(@class, 'list-unstyled')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "180"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id ='codesContent']//ul[contains(@class, 'list-unstyled')]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> SterlingCodifiersText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> CodeBookText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id ='mainContent']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }



        private static List<StepDefinition> OregonPortlandText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[@id ='main-content']"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EqualsToObject, null));

                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> KingCountyText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.SwitchToFrame));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "Frame"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='main-content-sr']//iframe"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "3"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//body"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

            defs.Add(new StepDefinition(Constants.StepType.GetElementText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentHtml"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> MaricopaCountyText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfStream));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentStream"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> SanDiegoText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "RootUrl"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.EndsWith, ".pdf"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetPdfStream));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentStream"));

                defs.Add(new StepDefinition(Constants.StepType.GetPdfText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ContentStream"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));
            }
            defs.Add(new StepDefinition(Constants.StepType.EndIf));

            defs.Add(new StepDefinition(Constants.StepType.If));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "RootUrl"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.NotEndsWith, ".pdf"));
            {
                defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//div[contains(@class, 'entry__content')]"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "Content"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementInnerHtml));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentHtml"));

                defs.Add(new StepDefinition(Constants.StepType.GetElementText));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "Content"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

                defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));
            }
            defs.Add(new StepDefinition(Constants.StepType.EndIf));


            return defs;
        }

        private static List<StepDefinition> EugeneText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "2"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfStream));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentStream"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> BoiseCityText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "2"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfStream));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentStream"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        private static List<StepDefinition> RiversideText()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "2"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfStream));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentStream"));

            defs.Add(new StepDefinition(Constants.StepType.GetPdfText));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputObject, "ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "ContentText"));

            defs.Add(new StepDefinition(Constants.StepType.MakeResultObject));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.InputResults, "RootUrl,ContentText,ContentStream"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddToResults, "ScrapedContent"));

            return defs;
        }

        #endregion

        #region Test percents

        private static List<StepDefinition> TestPercentCalculation()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "RootUrl"));

            defs.Add(new StepDefinition(Constants.StepType.SwitchToFrame));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.SwitchTo, "Frame"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//frame[@name='toc']"));

            // first level 1 loop
            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
            defs.Add(new StepDefinition(Constants.StepType.Wait));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));
            // Level 2 loop
            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
                defs.Add(new StepDefinition(Constants.StepType.Wait));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
        defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

        // Second level 1 loop
        defs.Add(new StepDefinition(Constants.StepType.ForEach));
        defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
            defs.Add(new StepDefinition(Constants.StepType.Wait));
            
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));
            // first level 2 loop
            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
                defs.Add(new StepDefinition(Constants.StepType.Wait));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
            // Second level 2 loop
            defs.Add(new StepDefinition(Constants.StepType.ForEach));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
                // Level 3 loop
                defs.Add(new StepDefinition(Constants.StepType.ForEach));
                defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//form/div[contains(@class, 'ptoc')]/p"));
                    defs.Add(new StepDefinition(Constants.StepType.Wait));
                    defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.WaitSeconds, "1"));
                defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
            defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));
                
        defs.Add(new StepDefinition(Constants.StepType.ForEachEnd));

        return defs;
    }

        #endregion

        #endregion

        private static void GenerateMigrationHelper(FlowStateSelenium fs)
        {
            using (System.IO.StreamWriter file =
                                   new System.IO.StreamWriter(@"D:\Development\FlowFactoryPOC\Results\MigrationHelper.txt", true))
            {
                file.WriteLine($"-- DefinitionName:\"{fs.DefinitionName}\"  Url:\"{fs.DefinitionUrl}\"  DefinitionType:\"{fs.DefinitionType}\"");
                foreach (var sd in fs.StepDefinitions)
                {
                    file.WriteLine($"exec dbo.GenericAlgorithm_InsertStep '{fs.DefinitionName}', '', '{fs.DefinitionUrl}', '{fs.DefinitionType}', 1, '{sd.TypeName}', {sd.StepNumber}, '{sd.Comment}';");

                    foreach (var p in sd.Params)
                    {
                        var pValue = string.IsNullOrEmpty(p.Value) ? p.Value : p.Value.Replace("'", "''");
                        if (pValue == null)
                            pValue = "NULL";
                        else
                            pValue = "'" + pValue + "'";
                        file.WriteLine($"exec dbo.GenericAlgorithm_InsertStepParameter '{fs.DefinitionName}', '{fs.DefinitionType}', '{sd.TypeName}', {sd.StepNumber}, '{p.Key}', {pValue};");
                    }
                }
            }
        }
    }
}
