﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var defs = new List<StepDefinition>();

            defs.Add(new StepDefinition(Constants.StepType.GoToUrl));

            defs.Add(new StepDefinition(Constants.StepType.GetElementByXPath));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.ByXPath, "//*[@id='tsf']/div[2]/div[3]/center/input[1]"));
            defs.Last().Params.Add(new KeyValuePair<string, string>(Constants.ParamType.AddOrUpdateResult, "SearchButton"));

            var defName = GetCurrentMethod();
            var defUrl = "http:google.com";
            var defType = "Locations";
            var flowState = new FlowStateSelenium(defName, defUrl, defType, defs, "http:google.com", 3, true);
            ExecuteAlgorithm(flowState);

            var result = flowState.Results.FirstOrDefault(x => x.Key == "SearchButton");
            Assert.IsTrue(result.Value != null);
            Assert.IsTrue(result.Value is IWebElement);

        }
    }
}
